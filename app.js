const express = require('express')
const path = require('path')
const app = express()
const router = require('./server/routes');
const proxy = require('http-proxy-middleware');
const bodyParser = require('body-parser');

const log4js = require('./server/middleware/logger');

//const mutipart= require('connect-multiparty');

//const mutipartMiddeware = mutipart();

log4js.use(app);
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

//app.use(mutipart({uploadDir:'./upload'}));


app.engine('html', require('express-art-template'))
app.use(express.static('client/dist'))
app.set('views', path.join(__dirname, 'client/dist/views'))
app.set('view options', {
	debug: process.env.NODE_ENV !== 'production'
})


if (process.env.NODE_ENV === "development") {
	app.use('/api', proxy({
		target: 'http://house.evergrande.cn', 
		changeOrigin: true,
		pathRewrite: {
			'^/api': '/api'
		}
	}));
}

app.use('/', router);

const server  = app.listen(8001);
console.log(`Node Sever up and runing on port on ${server.address().port}`)