import React, { Component } from 'react';
import { Row, Col } from 'antd';
import AddTodo from '../components/todos/addTodo';
import TodoList from '../components/todos/TodoList';
import '../scss/todos.scss';

export default class TodosPage extends Component {
    constructor (props) {
        super(props);
    }

    render () {
        return (
            <div className="todos-container">
                <Row>
                    <Col md={8}>
                        <AddTodo></AddTodo>
                        <TodoList></TodoList>
                    </Col>
                </Row>
            </div>
        )
    }
}
