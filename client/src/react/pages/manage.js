import React, { Component } from 'react';
import { Switch, Route, Link, Redirect } from 'react-router-dom';

import {  Layout, Menu, Icon } from 'antd';

import HomePage from './home';
import TodosPage from './todos';
import nav from '../data/nav';

const { Header, Content, Footer, Sider } = Layout;

const SubMenu = Menu.SubMenu;
const MenuItem = Menu.Item;

import '../scss/mange.scss';

export default class ManagePage extends Component {
    constructor (props) {
        super(props);
        this.state = {
            collapsed: false,
        }
        
        this.toggle = this.toggle.bind(this);
    }

    toggle () {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    render() {
        return (
            <Layout>
                <Sider
                    trigger={null}
                    collapsible
                    collapsed={this.state.collapsed}
                >
                    <Menu
                        theme="dark"
                        mode="inline"
                        inlineCollapsed={false}
                    >
                        {nav.map(iitem => (
                            <SubMenu key={iitem.key} title={<span><Icon type={iitem.icon} /><span>{iitem.title}</span></span>}>
                                {iitem.children.map(jitem => (
                                    <MenuItem key={jitem.key}>
                                        <Link to={this.props.match.path + jitem.path}>{jitem.title}</Link>
                                    </MenuItem>
                                ))}
                            </SubMenu>
                        ))}
                    </Menu>
                </Sider>
                <Layout>
                    <Header style={{ background: '#fff', padding: 0 }}>
                        <Icon
                            className="trigger"
                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                            onClick={this.toggle}
                        />
                    </Header>
                    <Content>
                        <Switch>
                            <Route path={this.props.match.path} exact component={HomePage}></Route>
                            <Route path={this.props.match.path + '/todos'} component={TodosPage}></Route>
                        </Switch>
                    </Content>
                </Layout>
            </Layout>
        );
    }
}