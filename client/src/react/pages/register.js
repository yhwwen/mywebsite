import React, { Component } from 'react';
import { Form, Icon, Input, Button, Row, Col } from 'antd';
const FormItem = Form.Item;
import { Link } from 'react-router-dom';
import md5 from 'md5';
import '../scss/register.scss';
import axios from 'axios';


const RegiterPage = () => <div>This is Regiter page.</div>;

class Register extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                //console.log('Received values of form: ', values);
                values.password = md5(values.password);
                console.log(values);
                axios.post('/user/register', values).then(res => {

                }).catch(err => {

                });
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 4 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 20 },
            },
        };

        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 20,
                    offset: 4,
                },
            },
        };
        
        return (
            <div className="register-page">
                <div className="register-box">
                    <header><h1>Register</h1></header>
                    <Form onSubmit={this.handleSubmit} className="register-form">
                        
                        <FormItem {...formItemLayout} label="Name">
                            {getFieldDecorator('name', {
                                rules: [
                                    {
                                        required: true,
                                        message: 'Please input your name'
                                    }
                                ]
                            })(
                                <Input />
                            )}
                        </FormItem>

                        <FormItem {...formItemLayout} label="Mobile">
                            {getFieldDecorator('mobile', {
                                rules: [
                                    {
                                        required: true,
                                        message: 'Please input your mobile'
                                    }, 
                                    {
                                        pattern: /^1\d{10}$/,
                                        message: 'Please input correct mobile'
                                    }
                                ]
                            })(
                                <Input />
                            )}
                        </FormItem>

                        <FormItem {...formItemLayout} label="Password">
                            {getFieldDecorator('password', {
                                rules: [
                                    {
                                        required: true,
                                        message: 'Please input your password'
                                    }, 
                                    {
                                        pattern: /^(?=.*\d)(?=.*[a-zA-Z])[\w\W]{6,20}$/,
                                        message: 'Please input correct password'
                                    }
                                ]
                            })(
                                <Input type="password" />
                            )}
                        </FormItem>
                        <FormItem {...tailFormItemLayout}>
                            <Button type="primary" htmlType="submit" className="Register-form-button">
                                Register
                            </Button> <span>Or <Link to="/react/login">login now!</Link></span>
                        </FormItem>
                    </Form>
                </div>  
            </div>
        );
    }
}

const RegisterPage = Form.create()(Register);

export default RegisterPage;