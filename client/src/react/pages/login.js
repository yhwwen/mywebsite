import React, { Component } from 'react';
import { Form, Icon, Input, Button, Row, Col } from 'antd';
import { Link } from 'react-router-dom';
const FormItem = Form.Item;
import md5 from 'md5';
import '../scss/login.scss';
import axios from 'axios';


class Login extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                values.password = md5(values.password);
                axios.post('/user/login', values).then(res => {
                    this.props.history.replace('/react');
                }).catch(err => {

                });
            }
        });
    }

    render () {
        const { getFieldDecorator } = this.props.form;
        return (
            <div className="login-page">
                <div className="login-box">
                    <header>
                        <h1>Login</h1>
                    </header>
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Row>
                            <Col span="24">
                                <FormItem>
                                {getFieldDecorator('name', {
                                    rules: [{ required: true, message: 'Please input your username!' }],
                                    })(
                                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                                )}
                                </FormItem>
                            </Col>
                        </Row>
                        <Row>
                            <Col span="24">
                                <FormItem>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: 'Please input your Password!' }],
                                })(
                                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                                )}
                                </FormItem>
                            </Col>
                        </Row>
                        <Row>
                            <Col span="24">
                                <FormItem>
                                    <Button type="primary" htmlType="submit" className="login-form-button">
                                        Log in
                                    </Button> <span>Or <Link to="/react/register">register now!</Link></span>
                                </FormItem>
                            </Col>
                        </Row>
                    </Form>
                </div>
            </div>
        );
    }
}

const LoginPage = Form.create()(Login);

export default LoginPage;