import React from 'react';
import { render } from 'react-dom';
import App from './App';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import appReducer from './reducers'
import 'antd/dist/antd.css';
let store = createStore(appReducer)



render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
)