import BaseLayout from "./components/baseLayout";
import { BrowserRouter } from 'react-router-dom';

import React, { Component } from 'react';
import './scss/common.scss';

const App =  () => (
    <BrowserRouter>
        <BaseLayout />
    </BrowserRouter>
);

export default App;