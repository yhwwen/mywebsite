const nav = [
    {
        icon: 'schedule',
        key: 'schedule',
        title: 'schedule',
        children: [
            {
                key: 'todos',
                title: 'todos',
                path: '/todos'
            }
        ]
    }
]

export default  nav;