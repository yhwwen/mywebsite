 import React, { Component } from 'react';
import { connect } from 'react-redux';
import { toggleTodo } from '../../actions/'
import PropTypes from 'prop-types';
import { List } from 'antd';



class TodoListUI extends Component {

    constructor(props) {
        super(props);
    }

    render () {
        return (
            <List
                bordered
                dataSource={this.props.todos}
                renderItem={item => (
                    <List.Item key={item.id}>{item.text}</List.Item>
                )}
            ></List>
        );
    }
}

TodoListUI.propTypes  = {
    todos: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        completed: PropTypes.bool.isRequired,
        text: PropTypes.string.isRequired
    }).isRequired).isRequired,
    onToggle: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    todos: state.todos
})

const mapDispatchToProps = dispatch => ({
    onToggle: id => {
        dispatch(toggleTodo(id))
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoListUI);
