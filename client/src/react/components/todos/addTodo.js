import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addTodo } from '../../actions/';
import PropTypes from 'prop-types';
import { Input } from 'antd';




class AddTodoUI extends Component {
    constructor (props) {
        super(props);
        
        this.state = {
            todoValue: ''
        };
        this.todoInput = null;
        
    }

    componentDidMount () {
        console.log(this.props);
        this.todoInput.input.focus();
    }

    handleSubmit (e) {
        let value = this.todoInput.input.value;
        if (!value.trim()) {
            return;
        }

        if (this.props.onPressEnter) {
            this.props.onPressEnter(value);
            this.setState({
                todoValue: ''
            })
        } 
    }

    handleChange (e) {
        this.setState({
            todoValue: this.todoInput.input.value
        })
    }

    render () {
        return (    
            <Input 
                ref={input => this.todoInput = input} 
                value={this.state.todoValue} 
                onChange={this.handleChange.bind(this)} 
                onPressEnter={this.handleSubmit.bind(this)} 
                placeholder="Please Enter anything you want to do next."
            />
        )
    }
}

AddTodoUI.propTypes  = {
    todos: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        completed: PropTypes.bool.isRequired,
        text: PropTypes.string.isRequired
    }).isRequired).isRequired,
    onPressEnter: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
        todos: state.todos
})

const mapDispatchToProps = dispatch => ({
    onPressEnter (value) {
        dispatch(addTodo(value))
    }
})


export default connect(mapStateToProps, mapDispatchToProps)(AddTodoUI)


