import React, { Component } from 'react';
import { Switch, Route, Link} from 'react-router-dom';
import ManagePage from '../pages/manage';

import LoginPage from '../pages/login';
import RegisterPage from '../pages/register';
import TodosPage from '../pages/todos';

const BaseLayout = () => (
    <div className="container">
        <Switch>
            <Route path="/react/login" component={LoginPage}></Route>
            <Route path="/react/register" component={RegisterPage}></Route>
            <Route path="/react" component={ManagePage}></Route>
        </Switch>
    </div>
);

export default BaseLayout;