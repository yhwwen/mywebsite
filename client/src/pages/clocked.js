import './clocked.scss';

let then = 0;
let delta = 1000;
let fps = 60;
let status = true;
let timeoutId = null;

const log2 = () => {
	console.log('foo');
}

const throttleLog = () => {
	let now = Date.now();
	if (now - then >= delta) {
		log2();
		then = now;
	}
}

const throttleLogFps = () => {
	let now = Date.now();
	if (1000 / (now - then) <= fps) {
		log2();
		then = now;
	}
}

const throttleLogStatus = () => {
	if (status) {
		log2();
		status = false;

		setTimeout(() => {
			status = true;
		}, delta)
	}
}

const debouncedLog = () => {
	clearTimeout(timeoutId)

	timeoutId = setTimeout(() => {
		log2();
	}, delta)
}

const log = () => {
	let now = Date.now();
	if (1000 / (now - then) > 60) {
		console.log("It's over 9000!!!");
	}

	then = now;
}

const immediatedLog = () => {
	if (status) {
		log2();
		status = false; 
	}

	clearTimeout(timeoutId);
	timeoutId = setTimeout(() => {
		status = true;
	}, delta)
}

const debounce = (fn, delta, context) => {
	let timeoutId = null;
	return (...args) => {
		clearTimeout(timeoutId);
		timeoutId = setTimeout(() => {
			fn.apply(context, args);
		}, delta)
	}
}

const immediate = (fn, delta, context) => {
	let timeoutId = null;
	let safe = true;

	return (...args) => {
		if (safe) {
			fn.apply(context, args);
			safe = true;
		}

		clearTimeout(timeoutId);
		timeoutId = setTimeout(() => {
			safe = true;
		}, delta)
	}
}

const throttle = (fn, delta, context) => {
	let safe = true;

	return (...args) => {
		if (safe) {
			fn.apply(context, args);
			safe = false;

			setTimeout(() => {
				safe = true;
			}, delta)
		}
	}
}

document.querySelector('#box01').onmousemove = log;
document.querySelector('#box02').onmousemove = throttleLog;
document.querySelector('#box03').onmousemove = throttleLogFps;
document.querySelector('#box04').onmousemove = throttleLogStatus;
document.querySelector('#text01').onkeydown = debouncedLog;
document.querySelector('#box05').onmousemove = immediatedLog;



