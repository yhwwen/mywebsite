import $ from 'jquery';
import i18next from 'i18next';
import Backend from 'i18next-xhr-backend';
import jqueryI18next  from 'jquery-i18next';





i18next.use(Backend).init({
    debug: true,
    ns: ['translation'],
    defaultNS: 'translation',
    lng: 'en',
    fallbackLng: 'zh_cn'
}, function(err, t) {
    jqueryI18next.init(i18next, $);
    $('[data-i18n]').localize();
});

