define("zepto", function(require, exports, t) {
    var _cacheThisModule_;
    var e = function() { var t, e, n, i, r = [],
            o = r.concat,
            a = r.filter,
            s = r.slice,
            u = window.document,
            f = {},
            c = {},
            l = { "column-count": 1, columns: 1, "font-weight": 1, "line-height": 1, opacity: 1, "z-index": 1, zoom: 1 },
            h = /^\s*<(\w+|!)[^>]*>/,
            p = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
            d = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
            m = /^(?:body|html)$/i,
            v = /([A-Z])/g,
            g = ["val", "css", "html", "text", "data", "width", "height", "offset"],
            y = ["after", "prepend", "before", "append"],
            b = u.createElement("table"),
            w = u.createElement("tr"),
            x = { tr: u.createElement("tbody"), tbody: b, thead: b, tfoot: b, td: w, th: w, "*": u.createElement("div") },
            E = /complete|loaded|interactive/,
            T = /^[\w-]*$/,
            S = {},
            j = S.toString,
            C = {},
            O, N, A = u.createElement("div"),
            P = { tabindex: "tabIndex", readonly: "readOnly", "for": "htmlFor", "class": "className", maxlength: "maxLength", cellspacing: "cellSpacing", cellpadding: "cellPadding", rowspan: "rowSpan", colspan: "colSpan", usemap: "useMap", frameborder: "frameBorder", contenteditable: "contentEditable" },
            M = Array.isArray || function(t) { return t instanceof Array };
        C.matches = function(t, e) { if (!e || !t || t.nodeType !== 1) return false; var n = t.matches || t.webkitMatchesSelector || t.mozMatchesSelector || t.oMatchesSelector || t.matchesSelector; if (n) return n.call(t, e); var i, r = t.parentNode,
                o = !r; if (o)(r = A).appendChild(t);
            i = ~C.qsa(r, e).indexOf(t);
            o && A.removeChild(t); return i };

        function D(t) { return t == null ? String(t) : S[j.call(t)] || "object" }

        function k(t) { return D(t) == "function" }

        function F(t) { return t != null && t == t.window }

        function L(t) { return t != null && t.nodeType == t.DOCUMENT_NODE }

        function $(t) { return D(t) == "object" }

        function R(t) { return $(t) && !F(t) && Object.getPrototypeOf(t) == Object.prototype }

        function _(t) { var e = !!t && "length" in t && t.length,
                i = n.type(t); return "function" != i && !F(t) && ("array" == i || e === 0 || typeof e == "number" && e > 0 && e - 1 in t) }

        function q(t) { return a.call(t, function(t) { return t != null }) }

        function z(t) { return t.length > 0 ? n.fn.concat.apply([], t) : t } O = function(t) { return t.replace(/-+(.)?/g, function(t, e) { return e ? e.toUpperCase() : "" }) };

        function I(t) { return t.replace(/::/g, "/").replace(/([A-Z]+)([A-Z][a-z])/g, "$1_$2").replace(/([a-z\d])([A-Z])/g, "$1_$2").replace(/_/g, "-").toLowerCase() } N = function(t) { return a.call(t, function(e, n) { return t.indexOf(e) == n }) };

        function W(t) { return t in c ? c[t] : c[t] = new RegExp("(^|\\s)" + t + "(\\s|$)") }

        function Z(t, e) { return typeof e == "number" && !l[I(t)] ? e + "px" : e }

        function B(t) { var e, n; if (!f[t]) { e = u.createElement(t);
                u.body.appendChild(e);
                n = getComputedStyle(e, "").getPropertyValue("display");
                e.parentNode.removeChild(e);
                n == "none" && (n = "block");
                f[t] = n } return f[t] }

        function V(t) { return "children" in t ? s.call(t.children) : n.map(t.childNodes, function(t) { if (t.nodeType == 1) return t }) }

        function H(t, e) { var n, i = t ? t.length : 0; for (n = 0; n < i; n++) this[n] = t[n];
            this.length = i;
            this.selector = e || "" } C.fragment = function(e, i, r) { var o, a, f; if (p.test(e)) o = n(u.createElement(RegExp.$1)); if (!o) { if (e.replace) e = e.replace(d, "<$1></$2>"); if (i === t) i = h.test(e) && RegExp.$1; if (!(i in x)) i = "*";
                f = x[i];
                f.innerHTML = "" + e;
                o = n.each(s.call(f.childNodes), function() { f.removeChild(this) }) } if (R(r)) { a = n(o);
                n.each(r, function(t, e) { if (g.indexOf(t) > -1) a[t](e);
                    else a.attr(t, e) }) } return o };
        C.Z = function(t, e) { return new H(t, e) };
        C.isZ = function(t) { return t instanceof C.Z };
        C.init = function(e, i) { var r; if (!e) return C.Z();
            else if (typeof e == "string") { e = e.trim(); if (e[0] == "<" && h.test(e)) r = C.fragment(e, RegExp.$1, i), e = null;
                else if (i !== t) return n(i).find(e);
                else r = C.qsa(u, e) } else if (k(e)) return n(u).ready(e);
            else if (C.isZ(e)) return e;
            else { if (M(e)) r = q(e);
                else if ($(e)) r = [e], e = null;
                else if (h.test(e)) r = C.fragment(e.trim(), RegExp.$1, i), e = null;
                else if (i !== t) return n(i).find(e);
                else r = C.qsa(u, e) } return C.Z(r, e) };
        n = function(t, e) { return C.init(t, e) };

        function U(n, i, r) { for (e in i)
                if (r && (R(i[e]) || M(i[e]))) { if (R(i[e]) && !R(n[e])) n[e] = {}; if (M(i[e]) && !M(n[e])) n[e] = [];
                    U(n[e], i[e], r) } else if (i[e] !== t) n[e] = i[e] } n.extend = function(t) { var e, n = s.call(arguments, 1); if (typeof t == "boolean") { e = t;
                t = n.shift() } n.forEach(function(n) { U(t, n, e) }); return t };
        C.qsa = function(t, e) { var n, i = e[0] == "#",
                r = !i && e[0] == ".",
                o = i || r ? e.slice(1) : e,
                a = T.test(o); return t.getElementById && a && i ? (n = t.getElementById(o)) ? [n] : [] : t.nodeType !== 1 && t.nodeType !== 9 && t.nodeType !== 11 ? [] : s.call(a && !i && t.getElementsByClassName ? r ? t.getElementsByClassName(o) : t.getElementsByTagName(e) : t.querySelectorAll(e)) };

        function X(t, e) { return e == null ? n(t) : n(t).filter(e) } n.contains = u.documentElement.contains ? function(t, e) { return t !== e && t.contains(e) } : function(t, e) { while (e && (e = e.parentNode))
                if (e === t) return true; return false };

        function Y(t, e, n, i) { return k(e) ? e.call(t, n, i) : e }

        function G(t, e, n) { n == null ? t.removeAttribute(e) : t.setAttribute(e, n) }

        function J(e, n) { var i = e.className || "",
                r = i && i.baseVal !== t; if (n === t) return r ? i.baseVal : i;
            r ? i.baseVal = n : e.className = n }

        function K(t) { try { return t ? t == "true" || (t == "false" ? false : t == "null" ? null : +t + "" == t ? +t : /^[\[\{]/.test(t) ? n.parseJSON(t) : t) : t } catch (e) { return t } } n.type = D;
        n.isFunction = k;
        n.isWindow = F;
        n.isArray = M;
        n.isPlainObject = R;
        n.isEmptyObject = function(t) { var e; for (e in t) return false; return true };
        n.isNumeric = function(t) { var e = Number(t),
                n = typeof t; return t != null && n != "boolean" && (n != "string" || t.length) && !isNaN(e) && isFinite(e) || false };
        n.inArray = function(t, e, n) { return r.indexOf.call(e, t, n) };
        n.camelCase = O;
        n.trim = function(t) { return t == null ? "" : String.prototype.trim.call(t) };
        n.uuid = 0;
        n.support = {};
        n.expr = {};
        n.noop = function() {};
        n.map = function(t, e) { var n, i = [],
                r, o; if (_(t))
                for (r = 0; r < t.length; r++) { n = e(t[r], r); if (n != null) i.push(n) } else
                    for (o in t) { n = e(t[o], o); if (n != null) i.push(n) }
            return z(i) };
        n.each = function(t, e) { var n, i; if (_(t)) { for (n = 0; n < t.length; n++)
                    if (e.call(t[n], n, t[n]) === false) return t } else { for (i in t)
                    if (e.call(t[i], i, t[i]) === false) return t } return t };
        n.grep = function(t, e) { return a.call(t, e) }; if (window.JSON) n.parseJSON = JSON.parse;
        n.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(t, e) { S["[object " + e + "]"] = e.toLowerCase() });
        n.fn = { constructor: C.Z, length: 0, forEach: r.forEach, reduce: r.reduce, push: r.push, sort: r.sort, splice: r.splice, indexOf: r.indexOf, concat: function() { var t, e, n = []; for (t = 0; t < arguments.length; t++) { e = arguments[t];
                    n[t] = C.isZ(e) ? e.toArray() : e } return o.apply(C.isZ(this) ? this.toArray() : this, n) }, map: function(t) { return n(n.map(this, function(e, n) { return t.call(e, n, e) })) }, slice: function() { return n(s.apply(this, arguments)) }, ready: function(t) { if (E.test(u.readyState) && u.body) t(n);
                else u.addEventListener("DOMContentLoaded", function() { t(n) }, false); return this }, get: function(e) { return e === t ? s.call(this) : this[e >= 0 ? e : e + this.length] }, toArray: function() { return this.get() }, size: function() { return this.length }, remove: function() { return this.each(function() { if (this.parentNode != null) this.parentNode.removeChild(this) }) }, each: function(t) { r.every.call(this, function(e, n) { return t.call(e, n, e) !== false }); return this }, filter: function(t) { if (k(t)) return this.not(this.not(t)); return n(a.call(this, function(e) { return C.matches(e, t) })) }, add: function(t, e) { return n(N(this.concat(n(t, e)))) }, is: function(t) { return this.length > 0 && C.matches(this[0], t) }, not: function(e) { var i = []; if (k(e) && e.call !== t) this.each(function(t) { if (!e.call(this, t)) i.push(this) });
                else { var r = typeof e == "string" ? this.filter(e) : _(e) && k(e.item) ? s.call(e) : n(e);
                    this.forEach(function(t) { if (r.indexOf(t) < 0) i.push(t) }) } return n(i) }, has: function(t) { return this.filter(function() { return $(t) ? n.contains(this, t) : n(this).find(t).size() }) }, eq: function(t) { return t === -1 ? this.slice(t) : this.slice(t, +t + 1) }, first: function() { var t = this[0]; return t && !$(t) ? t : n(t) }, last: function() { var t = this[this.length - 1]; return t && !$(t) ? t : n(t) }, find: function(t) { var e, i = this; if (!t) e = n();
                else if (typeof t == "object") e = n(t).filter(function() { var t = this; return r.some.call(i, function(e) { return n.contains(e, t) }) });
                else if (this.length == 1) e = n(C.qsa(this[0], t));
                else e = this.map(function() { return C.qsa(this, t) }); return e }, closest: function(t, e) { var i = [],
                    r = typeof t == "object" && n(t);
                this.each(function(n, o) { while (o && !(r ? r.indexOf(o) >= 0 : C.matches(o, t))) o = o !== e && !L(o) && o.parentNode; if (o && i.indexOf(o) < 0) i.push(o) }); return n(i) }, parents: function(t) { var e = [],
                    i = this; while (i.length > 0) i = n.map(i, function(t) { if ((t = t.parentNode) && !L(t) && e.indexOf(t) < 0) { e.push(t); return t } }); return X(e, t) }, parent: function(t) { return X(N(this.pluck("parentNode")), t) }, children: function(t) { return X(this.map(function() { return V(this) }), t) }, contents: function() { return this.map(function() { return this.contentDocument || s.call(this.childNodes) }) }, siblings: function(t) { return X(this.map(function(t, e) { return a.call(V(e.parentNode), function(t) { return t !== e }) }), t) }, empty: function() { return this.each(function() { this.innerHTML = "" }) }, pluck: function(t) { return n.map(this, function(e) { return e[t] }) }, show: function() { return this.each(function() { this.style.display == "none" && (this.style.display = ""); if (getComputedStyle(this, "").getPropertyValue("display") == "none") this.style.display = B(this.nodeName) }) }, replaceWith: function(t) { return this.before(t).remove() }, wrap: function(t) { var e = k(t); if (this[0] && !e) var i = n(t).get(0),
                    r = i.parentNode || this.length > 1; return this.each(function(o) { n(this).wrapAll(e ? t.call(this, o) : r ? i.cloneNode(true) : i) }) }, wrapAll: function(t) { if (this[0]) { n(this[0]).before(t = n(t)); var e; while ((e = t.children()).length) t = e.first();
                    n(t).append(this) } return this }, wrapInner: function(t) { var e = k(t); return this.each(function(i) { var r = n(this),
                        o = r.contents(),
                        a = e ? t.call(this, i) : t;
                    o.length ? o.wrapAll(a) : r.append(a) }) }, unwrap: function() { this.parent().each(function() { n(this).replaceWith(n(this).children()) }); return this }, clone: function() { return this.map(function() { return this.cloneNode(true) }) }, hide: function() { return this.css("display", "none") }, toggle: function(e) { return this.each(function() { var i = n(this);
                    (e === t ? i.css("display") == "none" : e) ? i.show(): i.hide() }) }, prev: function(t) { return n(this.pluck("previousElementSibling")).filter(t || "*") }, next: function(t) { return n(this.pluck("nextElementSibling")).filter(t || "*") }, html: function(t) { return 0 in arguments ? this.each(function(e) { var i = this.innerHTML;
                    n(this).empty().append(Y(this, t, e, i)) }) : 0 in this ? this[0].innerHTML : null }, text: function(t) { return 0 in arguments ? this.each(function(e) { var n = Y(this, t, e, this.textContent);
                    this.textContent = n == null ? "" : "" + n }) : 0 in this ? this.pluck("textContent").join("") : null }, attr: function(n, i) { var r; return typeof n == "string" && !(1 in arguments) ? 0 in this && this[0].nodeType == 1 && (r = this[0].getAttribute(n)) != null ? r : t : this.each(function(t) { if (this.nodeType !== 1) return; if ($(n))
                        for (e in n) G(this, e, n[e]);
                    else G(this, n, Y(this, i, t, this.getAttribute(n))) }) }, removeAttr: function(t) { return this.each(function() { this.nodeType === 1 && t.split(" ").forEach(function(t) { G(this, t) }, this) }) }, prop: function(t, e) { t = P[t] || t; return 1 in arguments ? this.each(function(n) { this[t] = Y(this, e, n, this[t]) }) : this[0] && this[0][t] }, removeProp: function(t) { t = P[t] || t; return this.each(function() { delete this[t] }) }, data: function(e, n) { var i = "data-" + e.replace(v, "-$1").toLowerCase(); var r = 1 in arguments ? this.attr(i, n) : this.attr(i); return r !== null ? K(r) : t }, val: function(t) { if (0 in arguments) { if (t == null) t = ""; return this.each(function(e) { this.value = Y(this, t, e, this.value) }) } else { return this[0] && (this[0].multiple ? n(this[0]).find("option").filter(function() { return this.selected }).pluck("value") : this[0].value) } }, offset: function(t) { if (t) return this.each(function(e) { var i = n(this),
                        r = Y(this, t, e, i.offset()),
                        o = i.offsetParent().offset(),
                        a = { top: r.top - o.top, left: r.left - o.left }; if (i.css("position") == "static") a["position"] = "relative";
                    i.css(a) }); if (!this.length) return null; if (u.documentElement !== this[0] && !n.contains(u.documentElement, this[0])) return { top: 0, left: 0 }; var e = this[0].getBoundingClientRect(); return { left: e.left + window.pageXOffset, top: e.top + window.pageYOffset, width: Math.round(e.width), height: Math.round(e.height) } }, css: function(t, i) { if (arguments.length < 2) { var r = this[0]; if (typeof t == "string") { if (!r) return; return r.style[O(t)] || getComputedStyle(r, "").getPropertyValue(t) } else if (M(t)) { if (!r) return; var o = {}; var a = getComputedStyle(r, "");
                        n.each(t, function(t, e) { o[e] = r.style[O(e)] || a.getPropertyValue(e) }); return o } } var s = ""; if (D(t) == "string") { if (!i && i !== 0) this.each(function() { this.style.removeProperty(I(t)) });
                    else s = I(t) + ":" + Z(t, i) } else { for (e in t)
                        if (!t[e] && t[e] !== 0) this.each(function() { this.style.removeProperty(I(e)) });
                        else s += I(e) + ":" + Z(e, t[e]) + ";" } return this.each(function() { this.style.cssText += ";" + s }) }, index: function(t) { return t ? this.indexOf(n(t)[0]) : this.parent().children().indexOf(this[0]) }, hasClass: function(t) { if (!t) return false; return r.some.call(this, function(t) { return this.test(J(t)) }, W(t)) }, addClass: function(t) { if (!t) return this; return this.each(function(e) { if (!("className" in this)) return;
                    i = []; var r = J(this),
                        o = Y(this, t, e, r);
                    o.split(/\s+/g).forEach(function(t) { if (!n(this).hasClass(t)) i.push(t) }, this);
                    i.length && J(this, r + (r ? " " : "") + i.join(" ")) }) }, removeClass: function(e) { return this.each(function(n) { if (!("className" in this)) return; if (e === t) return J(this, "");
                    i = J(this);
                    Y(this, e, n, i).split(/\s+/g).forEach(function(t) { i = i.replace(W(t), " ") });
                    J(this, i.trim()) }) }, toggleClass: function(e, i) { if (!e) return this; return this.each(function(r) { var o = n(this),
                        a = Y(this, e, r, J(this));
                    a.split(/\s+/g).forEach(function(e) {
                        (i === t ? !o.hasClass(e) : i) ? o.addClass(e): o.removeClass(e) }) }) }, scrollTop: function(e) { if (!this.length) return; var n = "scrollTop" in this[0]; if (e === t) return n ? this[0].scrollTop : this[0].pageYOffset; return this.each(n ? function() { this.scrollTop = e } : function() { this.scrollTo(this.scrollX, e) }) }, scrollLeft: function(e) { if (!this.length) return; var n = "scrollLeft" in this[0]; if (e === t) return n ? this[0].scrollLeft : this[0].pageXOffset; return this.each(n ? function() { this.scrollLeft = e } : function() { this.scrollTo(e, this.scrollY) }) }, position: function() { if (!this.length) return; var t = this[0],
                    e = this.offsetParent(),
                    i = this.offset(),
                    r = m.test(e[0].nodeName) ? { top: 0, left: 0 } : e.offset();
                i.top -= parseFloat(n(t).css("margin-top")) || 0;
                i.left -= parseFloat(n(t).css("margin-left")) || 0;
                r.top += parseFloat(n(e[0]).css("border-top-width")) || 0;
                r.left += parseFloat(n(e[0]).css("border-left-width")) || 0; return { top: i.top - r.top, left: i.left - r.left } }, offsetParent: function() { return this.map(function() { var t = this.offsetParent || u.body; while (t && !m.test(t.nodeName) && n(t).css("position") == "static") t = t.offsetParent; return t }) } };
        n.fn.detach = n.fn.remove;
        ["width", "height"].forEach(function(e) { var i = e.replace(/./, function(t) { return t[0].toUpperCase() });
            n.fn[e] = function(r) { var o, a = this[0]; if (r === t) return F(a) ? a["inner" + i] : L(a) ? a.documentElement["scroll" + i] : (o = this.offset()) && o[e];
                else return this.each(function(t) { a = n(this);
                    a.css(e, Y(this, r, t, a[e]())) }) } });

        function Q(t, e) { e(t); for (var n = 0, i = t.childNodes.length; n < i; n++) Q(t.childNodes[n], e) } y.forEach(function(e, i) { var r = i % 2;
            n.fn[e] = function() { var e, o = n.map(arguments, function(i) { var r = [];
                        e = D(i); if (e == "array") { i.forEach(function(e) { if (e.nodeType !== t) return r.push(e);
                                else if (n.zepto.isZ(e)) return r = r.concat(e.get());
                                r = r.concat(C.fragment(e)) }); return r } return e == "object" || i == null ? i : C.fragment(i) }),
                    a, s = this.length > 1; if (o.length < 1) return this; return this.each(function(t, e) { a = r ? e : e.parentNode;
                    e = i == 0 ? e.nextSibling : i == 1 ? e.firstChild : i == 2 ? e : null; var f = n.contains(u.documentElement, a);
                    o.forEach(function(t) { if (s) t = t.cloneNode(true);
                        else if (!a) return n(t).remove();
                        a.insertBefore(t, e); if (f) Q(t, function(t) { if (t.nodeName != null && t.nodeName.toUpperCase() === "SCRIPT" && (!t.type || t.type === "text/javascript") && !t.src) { var e = t.ownerDocument ? t.ownerDocument.defaultView : window;
                                e["eval"].call(e, t.innerHTML) } }) }) }) };
            n.fn[r ? e + "To" : "insert" + (i ? "Before" : "After")] = function(t) { n(t)[e](this); return this } });
        C.Z.prototype = H.prototype = n.fn;
        C.uniq = N;
        C.deserializeValue = K;
        n.zepto = C; return n }();
    window.Zepto = e;
    window.$ === undefined && (window.$ = e);
    (function(t) { var e = 1,
            n, i = Array.prototype.slice,
            r = t.isFunction,
            o = function(t) { return typeof t == "string" },
            a = {},
            s = {},
            u = "onfocusin" in window,
            f = { focus: "focusin", blur: "focusout" },
            c = { mouseenter: "mouseover", mouseleave: "mouseout" };
        s.click = s.mousedown = s.mouseup = s.mousemove = "MouseEvents";

        function l(t) { return t._zid || (t._zid = e++) }

        function h(t, e, n, i) { e = p(e); if (e.ns) var r = d(e.ns); return (a[l(t)] || []).filter(function(t) { return t && (!e.e || t.e == e.e) && (!e.ns || r.test(t.ns)) && (!n || l(t.fn) === l(n)) && (!i || t.sel == i) }) }

        function p(t) { var e = ("" + t).split("."); return { e: e[0], ns: e.slice(1).sort().join(" ") } }

        function d(t) { return new RegExp("(?:^| )" + t.replace(" ", " .* ?") + "(?: |$)") }

        function m(t, e) { return t.del && (!u && t.e in f) || !!e }

        function v(t) { return c[t] || u && f[t] || t }

        function g(e, i, r, o, s, u, f) { var h = l(e),
                d = a[h] || (a[h] = []);
            i.split(/\s/).forEach(function(i) { if (i == "ready") return t(document).ready(r); var a = p(i);
                a.fn = r;
                a.sel = s; if (a.e in c) r = function(e) { var n = e.relatedTarget; if (!n || n !== this && !t.contains(this, n)) return a.fn.apply(this, arguments) };
                a.del = u; var l = u || r;
                a.proxy = function(t) { t = T(t); if (t.isImmediatePropagationStopped()) return;
                    t.data = o; var i = l.apply(e, t._args == n ? [t] : [t].concat(t._args)); if (i === false) t.preventDefault(), t.stopPropagation(); return i };
                a.i = d.length;
                d.push(a); if ("addEventListener" in e) e.addEventListener(v(a.e), a.proxy, m(a, f)) }) }

        function y(t, e, n, i, r) { var o = l(t);
            (e || "").split(/\s/).forEach(function(e) { h(t, e, n, i).forEach(function(e) { delete a[o][e.i]; if ("removeEventListener" in t) t.removeEventListener(v(e.e), e.proxy, m(e, r)) }) }) } t.event = { add: g, remove: y };
        t.proxy = function(e, n) { var a = 2 in arguments && i.call(arguments, 2); if (r(e)) { var s = function() { return e.apply(n, a ? a.concat(i.call(arguments)) : arguments) };
                s._zid = l(e); return s } else if (o(n)) { if (a) { a.unshift(e[n], e); return t.proxy.apply(null, a) } else { return t.proxy(e[n], e) } } else { throw new TypeError("expected function") } };
        t.fn.bind = function(t, e, n) { return this.on(t, e, n) };
        t.fn.unbind = function(t, e) { return this.off(t, e) };
        t.fn.one = function(t, e, n, i) { return this.on(t, e, n, i, 1) }; var b = function() { return true },
            w = function() { return false },
            x = /^([A-Z]|returnValue$|layer[XY]$|webkitMovement[XY]$)/,
            E = { preventDefault: "isDefaultPrevented", stopImmediatePropagation: "isImmediatePropagationStopped", stopPropagation: "isPropagationStopped" };

        function T(e, i) { if (i || !e.isDefaultPrevented) { i || (i = e);
                t.each(E, function(t, n) { var r = i[t];
                    e[t] = function() { this[n] = b; return r && r.apply(i, arguments) };
                    e[n] = w });
                e.timeStamp || (e.timeStamp = Date.now()); if (i.defaultPrevented !== n ? i.defaultPrevented : "returnValue" in i ? i.returnValue === false : i.getPreventDefault && i.getPreventDefault()) e.isDefaultPrevented = b } return e }

        function S(t) { var e, i = { originalEvent: t }; for (e in t)
                if (!x.test(e) && t[e] !== n) i[e] = t[e]; return T(i, t) } t.fn.delegate = function(t, e, n) { return this.on(e, t, n) };
        t.fn.undelegate = function(t, e, n) { return this.off(e, t, n) };
        t.fn.live = function(e, n) { t(document.body).delegate(this.selector, e, n); return this };
        t.fn.die = function(e, n) { t(document.body).undelegate(this.selector, e, n); return this };
        t.fn.on = function(e, a, s, u, f) { var c, l, h = this; if (e && !o(e)) { t.each(e, function(t, e) { h.on(t, a, s, e, f) }); return h } if (!o(a) && !r(u) && u !== false) u = s, s = a, a = n; if (u === n || s === false) u = s, s = n; if (u === false) u = w; return h.each(function(n, r) { if (f) c = function(t) { y(r, t.type, u); return u.apply(this, arguments) }; if (a) l = function(e) { var n, o = t(e.target).closest(a, r).get(0); if (o && o !== r) { n = t.extend(S(e), { currentTarget: o, liveFired: r }); return (c || u).apply(o, [n].concat(i.call(arguments, 1))) } };
                g(r, e, u, s, a, l || c) }) };
        t.fn.off = function(e, i, a) { var s = this; if (e && !o(e)) { t.each(e, function(t, e) { s.off(t, i, e) }); return s } if (!o(i) && !r(a) && a !== false) a = i, i = n; if (a === false) a = w; return s.each(function() { y(this, e, a, i) }) };
        t.fn.trigger = function(e, n) { e = o(e) || t.isPlainObject(e) ? t.Event(e) : T(e);
            e._args = n; return this.each(function() { if (e.type in f && typeof this[e.type] == "function") this[e.type]();
                else if ("dispatchEvent" in this) this.dispatchEvent(e);
                else t(this).triggerHandler(e, n) }) };
        t.fn.triggerHandler = function(e, n) { var i, r;
            this.each(function(a, s) { i = S(o(e) ? t.Event(e) : e);
                i._args = n;
                i.target = s;
                t.each(h(s, e.type || e), function(t, e) { r = e.proxy(i); if (i.isImmediatePropagationStopped()) return false }) }); return r };
        ("focusin focusout focus blur load resize scroll unload click dblclick " + "mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " + "change select keydown keypress keyup error").split(" ").forEach(function(e) { t.fn[e] = function(t) { return 0 in arguments ? this.bind(e, t) : this.trigger(e) } });
        t.Event = function(t, e) { if (!o(t)) e = t, t = e.type; var n = document.createEvent(s[t] || "Events"),
                i = true; if (e)
                for (var r in e) r == "bubbles" ? i = !!e[r] : n[r] = e[r];
            n.initEvent(t, i, true); return T(n) } })(e);
    (function(t) { var e = +new Date,
            n = window.document,
            i, r, o = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
            a = /^(?:text|application)\/javascript/i,
            s = /^(?:text|application)\/xml/i,
            u = "application/json",
            f = "text/html",
            c = /^\s*$/,
            l = n.createElement("a");
        l.href = window.location.href;

        function h(e, n, i) { var r = t.Event(n);
            t(e).trigger(r, i); return !r.isDefaultPrevented() }

        function p(t, e, i, r) { if (t.global) return h(e || n, i, r) } t.active = 0;

        function d(e) { if (e.global && t.active++ === 0) p(e, null, "ajaxStart") }

        function m(e) { if (e.global && !--t.active) p(e, null, "ajaxStop") }

        function v(t, e) { var n = e.context; if (e.beforeSend.call(n, t, e) === false || p(e, n, "ajaxBeforeSend", [t, e]) === false) return false;
            p(e, n, "ajaxSend", [t, e]) }

        function g(t, e, n, i) { var r = n.context,
                o = "success";
            n.success.call(r, t, o, e); if (i) i.resolveWith(r, [t, o, e]);
            p(n, r, "ajaxSuccess", [e, n, t]);
            b(o, e, n) }

        function y(t, e, n, i, r) { var o = i.context;
            i.error.call(o, n, e, t); if (r) r.rejectWith(o, [n, e, t]);
            p(i, o, "ajaxError", [n, i, t || e]);
            b(e, n, i) }

        function b(t, e, n) { var i = n.context;
            n.complete.call(i, e, t);
            p(n, i, "ajaxComplete", [e, n]);
            m(n) }

        function w(t, e, n) { if (n.dataFilter == x) return t; var i = n.context; return n.dataFilter.call(i, t, e) }

        function x() {} t.ajaxJSONP = function(i, r) { if (!("type" in i)) return t.ajax(i); var o = i.jsonpCallback,
                a = (t.isFunction(o) ? o() : o) || "Zepto" + e++,
                s = n.createElement("script"),
                u = window[a],
                f, c = function(e) { t(s).triggerHandler("error", e || "abort") },
                l = { abort: c },
                h; if (r) r.promise(l);
            t(s).on("load error", function(e, n) { clearTimeout(h);
                t(s).off().remove(); if (e.type == "error" || !f) { y(null, n || "error", l, i, r) } else { g(f[0], l, i, r) } window[a] = u; if (f && t.isFunction(u)) u(f[0]);
                u = f = undefined }); if (v(l, i) === false) { c("abort"); return l } window[a] = function() { f = arguments };
            s.src = i.url.replace(/\?(.+)=\?/, "?$1=" + a);
            n.head.appendChild(s); if (i.timeout > 0) h = setTimeout(function() { c("timeout") }, i.timeout); return l };
        t.ajaxSettings = { type: "GET", beforeSend: x, success: x, error: x, complete: x, context: null, global: true, xhr: function() { return new window.XMLHttpRequest }, accepts: { script: "text/javascript, application/javascript, application/x-javascript", json: u, xml: "application/xml, text/xml", html: f, text: "text/plain" }, crossDomain: false, timeout: 0, processData: true, cache: true, dataFilter: x };

        function E(t) { if (t) t = t.split(";", 2)[0]; return t && (t == f ? "html" : t == u ? "json" : a.test(t) ? "script" : s.test(t) && "xml") || "text" }

        function T(t, e) { if (e == "") return t; return (t + "&" + e).replace(/[&?]{1,2}/, "?") }

        function S(e) { if (e.processData && e.data && t.type(e.data) != "string") e.data = t.param(e.data, e.traditional); if (e.data && (!e.type || e.type.toUpperCase() == "GET" || "jsonp" == e.dataType)) e.url = T(e.url, e.data), e.data = undefined } t.ajax = function(e) { var o = t.extend({}, e || {}),
                a = t.Deferred && t.Deferred(),
                s, u; for (i in t.ajaxSettings)
                if (o[i] === undefined) o[i] = t.ajaxSettings[i];
            d(o); if (!o.crossDomain) { s = n.createElement("a");
                s.href = o.url;
                s.href = s.href;
                o.crossDomain = l.protocol + "//" + l.host !== s.protocol + "//" + s.host } if (!o.url) o.url = window.location.toString(); if ((u = o.url.indexOf("#")) > -1) o.url = o.url.slice(0, u);
            S(o); var f = o.dataType,
                h = /\?.+=\?/.test(o.url); if (h) f = "jsonp"; if (o.cache === false || (!e || e.cache !== true) && ("script" == f || "jsonp" == f)) o.url = T(o.url, "_=" + Date.now()); if ("jsonp" == f) { if (!h) o.url = T(o.url, o.jsonp ? o.jsonp + "=?" : o.jsonp === false ? "" : "callback=?"); return t.ajaxJSONP(o, a) } var p = o.accepts[f],
                m = {},
                b = function(t, e) { m[t.toLowerCase()] = [t, e] },
                j = /^([\w-]+:)\/\//.test(o.url) ? RegExp.$1 : window.location.protocol,
                C = o.xhr(),
                O = C.setRequestHeader,
                N; if (a) a.promise(C); if (!o.crossDomain) b("X-Requested-With", "XMLHttpRequest");
            b("Accept", p || "*/*"); if (p = o.mimeType || p) { if (p.indexOf(",") > -1) p = p.split(",", 2)[0];
                C.overrideMimeType && C.overrideMimeType(p) } if (o.contentType || o.contentType !== false && o.data && o.type.toUpperCase() != "GET") b("Content-Type", o.contentType || "application/x-www-form-urlencoded"); if (o.headers)
                for (r in o.headers) b(r, o.headers[r]);
            C.setRequestHeader = b;
            C.onreadystatechange = function() { if (C.readyState == 4) { C.onreadystatechange = x;
                    clearTimeout(N); var e, n = false; if (C.status >= 200 && C.status < 300 || C.status == 304 || C.status == 0 && j == "file:") { f = f || E(o.mimeType || C.getResponseHeader("content-type")); if (C.responseType == "arraybuffer" || C.responseType == "blob") e = C.response;
                        else { e = C.responseText; try { e = w(e, f, o); if (f == "script")(1, eval)(e);
                                else if (f == "xml") e = C.responseXML;
                                else if (f == "json") e = c.test(e) ? null : t.parseJSON(e) } catch (i) { n = i } if (n) return y(n, "parsererror", C, o, a) } g(e, C, o, a) } else { y(C.statusText || null, C.status ? "error" : "abort", C, o, a) } } }; if (v(C, o) === false) { C.abort();
                y(null, "abort", C, o, a); return C } var A = "async" in o ? o.async : true;
            C.open(o.type, o.url, A, o.username, o.password); if (o.xhrFields)
                for (r in o.xhrFields) C[r] = o.xhrFields[r]; for (r in m) O.apply(C, m[r]); if (o.timeout > 0) N = setTimeout(function() { C.onreadystatechange = x;
                C.abort();
                y(null, "timeout", C, o, a) }, o.timeout);
            C.send(o.data ? o.data : null); return C };

        function j(e, n, i, r) { if (t.isFunction(n)) r = i, i = n, n = undefined; if (!t.isFunction(i)) r = i, i = undefined; return { url: e, data: n, success: i, dataType: r } } t.get = function() { return t.ajax(j.apply(null, arguments)) };
        t.post = function() { var e = j.apply(null, arguments);
            e.type = "POST"; return t.ajax(e) };
        t.getJSON = function() { var e = j.apply(null, arguments);
            e.dataType = "json"; return t.ajax(e) };
        t.fn.load = function(e, n, i) { if (!this.length) return this; var r = this,
                a = e.split(/\s/),
                s, u = j(e, n, i),
                f = u.success; if (a.length > 1) u.url = a[0], s = a[1];
            u.success = function(e) { r.html(s ? t("<div>").html(e.replace(o, "")).find(s) : e);
                f && f.apply(r, arguments) };
            t.ajax(u); return this }; var C = encodeURIComponent;

        function O(e, n, i, r) { var o, a = t.isArray(n),
                s = t.isPlainObject(n);
            t.each(n, function(n, u) { o = t.type(u); if (r) n = i ? r : r + "[" + (s || o == "object" || o == "array" ? n : "") + "]"; if (!r && a) e.add(u.name, u.value);
                else if (o == "array" || !i && o == "object") O(e, u, i, n);
                else e.add(n, u) }) } t.param = function(e, n) { var i = [];
            i.add = function(e, n) { if (t.isFunction(n)) n = n(); if (n == null) n = "";
                this.push(C(e) + "=" + C(n)) };
            O(i, e, n); return i.join("&").replace(/%20/g, "+") } })(e);
    (function(t) { t.fn.serializeArray = function() { var e, n, i = [],
                r = function(t) { if (t.forEach) return t.forEach(r);
                    i.push({ name: e, value: t }) }; if (this[0]) t.each(this[0].elements, function(i, o) { n = o.type, e = o.name; if (e && o.nodeName.toLowerCase() != "fieldset" && !o.disabled && n != "submit" && n != "reset" && n != "button" && n != "file" && (n != "radio" && n != "checkbox" || o.checked)) r(t(o).val()) }); return i };
        t.fn.serialize = function() { var t = [];
            this.serializeArray().forEach(function(e) { t.push(encodeURIComponent(e.name) + "=" + encodeURIComponent(e.value)) }); return t.join("&") };
        t.fn.submit = function(e) { if (0 in arguments) this.bind("submit", e);
            else if (this.length) { var n = t.Event("submit");
                this.eq(0).trigger(n); if (!n.isDefaultPrevented()) this.get(0).submit() } return this } })(e);
    (function() { try { getComputedStyle(undefined) } catch (t) { var e = getComputedStyle;
            window.getComputedStyle = function(t, n) { try { return e(t, n) } catch (i) { return null } } } })();
    (function(t) {
        function e(t, e) { var n = this.os = {},
                i = this.browser = {},
                r = t.match(/Web[kK]it[\/]{0,1}([\d.]+)/),
                o = t.match(/(Android);?[\s\/]+([\d.]+)?/),
                a = !!t.match(/\(Macintosh\; Intel /),
                s = t.match(/(iPad).*OS\s([\d_]+)/),
                u = t.match(/(iPod)(.*OS\s([\d_]+))?/),
                f = !s && t.match(/(iPhone\sOS)\s([\d_]+)/),
                c = t.match(/(webOS|hpwOS)[\s\/]([\d.]+)/),
                l = /Win\d{2}|Windows/.test(e),
                h = t.match(/Windows Phone ([\d.]+)/),
                p = c && t.match(/TouchPad/),
                d = t.match(/Kindle\/([\d.]+)/),
                m = t.match(/Silk\/([\d._]+)/),
                v = t.match(/(BlackBerry).*Version\/([\d.]+)/),
                g = t.match(/(BB10).*Version\/([\d.]+)/),
                y = t.match(/(RIM\sTablet\sOS)\s([\d.]+)/),
                b = t.match(/PlayBook/),
                w = t.match(/Chrome\/([\d.]+)/) || t.match(/CriOS\/([\d.]+)/),
                x = t.match(/Firefox\/([\d.]+)/),
                E = t.match(/\((?:Mobile|Tablet); rv:([\d.]+)\).*Firefox\/[\d.]+/),
                T = t.match(/MSIE\s([\d.]+)/) || t.match(/Trident\/[\d](?=[^\?]+).*rv:([0-9.].)/),
                S = !w && t.match(/(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/),
                j = S || t.match(/Version\/([\d.]+)([^S](Safari)|[^M]*(Mobile)[^S]*(Safari))/); if (i.webkit = !!r) i.version = r[1]; if (o) n.android = true, n.version = o[2]; if (f && !u) n.ios = n.iphone = true, n.version = f[2].replace(/_/g, "."); if (s) n.ios = n.ipad = true, n.version = s[2].replace(/_/g, "."); if (u) n.ios = n.ipod = true, n.version = u[3] ? u[3].replace(/_/g, ".") : null; if (h) n.wp = true, n.version = h[1]; if (c) n.webos = true, n.version = c[2]; if (p) n.touchpad = true; if (v) n.blackberry = true, n.version = v[2]; if (g) n.bb10 = true, n.version = g[2]; if (y) n.rimtabletos = true, n.version = y[2]; if (b) i.playbook = true; if (d) n.kindle = true, n.version = d[1]; if (m) i.silk = true, i.version = m[1]; if (!m && n.android && t.match(/Kindle Fire/)) i.silk = true; if (w) i.chrome = true, i.version = w[1]; if (x) i.firefox = true, i.version = x[1]; if (E) n.firefoxos = true, n.version = E[1]; if (T) i.ie = true, i.version = T[1]; if (j && (a || n.ios || l)) { i.safari = true; if (!n.ios) i.version = j[1] } if (S) i.webview = true;
            n.tablet = !!(s || b || o && !t.match(/Mobile/) || x && t.match(/Tablet/) || T && !t.match(/Phone/) && t.match(/Touch/));
            n.phone = !!(!n.tablet && !n.ipod && (o || f || c || v || g || w && t.match(/Android/) || w && t.match(/CriOS\/([\d.]+)/) || x && t.match(/Mobile/) || T && t.match(/Touch/))) } e.call(t, navigator.userAgent, navigator.platform);
        t.__detect = e })(e);
    (function(t) { var e = [],
            n;
        t.fn.remove = function() { return this.each(function() { if (this.parentNode) { if (this.tagName === "IMG") { e.push(this);
                        this.src = "data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="; if (n) clearTimeout(n);
                        n = setTimeout(function() { e = [] }, 6e4) } this.parentNode.removeChild(this) } }) } })(e);
    (function(t) { var e = {},
            n = t.fn.data,
            i = t.camelCase,
            r = t.expando = "Zepto" + +new Date,
            o = [];

        function a(o, a) { var u = o[r],
                f = u && e[u]; if (a === undefined) return f || s(o);
            else { if (f) { if (a in f) return f[a]; var c = i(a); if (c in f) return f[c] } return n.call(t(o), a) } }

        function s(n, o, a) { var s = n[r] || (n[r] = ++t.uuid),
                f = e[s] || (e[s] = u(n)); if (o !== undefined) f[i(o)] = a; return f }

        function u(e) { var n = {};
            t.each(e.attributes || o, function(e, r) { if (r.name.indexOf("data-") == 0) n[i(r.name.replace("data-", ""))] = t.zepto.deserializeValue(r.value) }); return n } t.fn.data = function(e, n) { return n === undefined ? t.isPlainObject(e) ? this.each(function(n, i) { t.each(e, function(t, e) { s(i, t, e) }) }) : 0 in this ? a(this[0], e) : undefined : this.each(function() { s(this, e, n) }) };
        t.data = function(e, n, i) { return t(e).data(n, i) };
        t.hasData = function(n) { var i = n[r],
                o = i && e[i]; return o ? !t.isEmptyObject(o) : false };
        t.fn.removeData = function(n) { if (typeof n == "string") n = n.split(/\s+/); return this.each(function() { var o = this[r],
                    a = o && e[o]; if (a) t.each(n || a, function(t) { delete a[n ? i(this) : t] }) }) };
        ["remove", "empty"].forEach(function(e) { var n = t.fn[e];
            t.fn[e] = function() { var t = this.find("*"); if (e === "remove") t = t.add(this);
                t.removeData(); return n.call(this) } }) })(e);
    (function(t) {
        var e = Array.prototype.slice;

        function n(e) { var i = [
                    ["resolve", "done", t.Callbacks({ once: 1, memory: 1 }), "resolved"],
                    ["reject", "fail", t.Callbacks({ once: 1, memory: 1 }), "rejected"],
                    ["notify", "progress", t.Callbacks({ memory: 1 })]
                ],
                r = "pending",
                o = { state: function() { return r }, always: function() { a.done(arguments).fail(arguments); return this }, then: function() { var e = arguments; return n(function(n) { t.each(i, function(i, r) { var s = t.isFunction(e[i]) && e[i];
                                a[r[1]](function() { var e = s && s.apply(this, arguments); if (e && t.isFunction(e.promise)) { e.promise().done(n.resolve).fail(n.reject).progress(n.notify) } else { var i = this === o ? n.promise() : this,
                                            a = s ? [e] : arguments;
                                        n[r[0] + "With"](i, a) } }) });
                            e = null }).promise() }, promise: function(e) { return e != null ? t.extend(e, o) : o } },
                a = {};
            t.each(i, function(t, e) { var n = e[2],
                    s = e[3];
                o[e[1]] = n.add; if (s) { n.add(function() { r = s }, i[t ^ 1][2].disable, i[2][2].lock) } a[e[0]] = function() { a[e[0] + "With"](this === a ? o : this, arguments); return this };
                a[e[0] + "With"] = n.fireWith });
            o.promise(a); if (e) e.call(a, a); return a } t.when = function(i) {
            var r = e.call(arguments),
                o = r.length,
                a = 0,
                s = o !== 1 || i && t.isFunction(i.promise) ? o : 0,
                u = s === 1 ? i : n(),
                f, c, l, h = function(t, n, i) { return function(r) { n[t] = this;
                        i[t] = arguments.length > 1 ? e.call(arguments) : r; if (i === f) { u.notifyWith(n, i) } else if (!--s) { u.resolveWith(n, i) } } };
            if (o > 1) {
                f = new Array(o);
                c = new Array(o);
                l = new Array(o);
                for (; a < o; ++a) {
                    if (r[a] && t.isFunction(r[a].promise)) { r[a].promise().done(h(a, l, r)).fail(u.reject).progress(h(a, c, f)) } else {
                        --s
                    }
                }
            }
            if (!s) u.resolveWith(l, r);
            return u.promise()
        };
        t.Deferred = n
    })(e);
    (function(t) { t.Callbacks = function(e) { e = t.extend({}, e); var n, i, r, o, a, s, u = [],
                f = !e.once && [],
                c = function(t) { n = e.memory && t;
                    i = true;
                    s = o || 0;
                    o = 0;
                    a = u.length;
                    r = true; for (; u && s < a; ++s) { if (u[s].apply(t[0], t[1]) === false && e.stopOnFalse) { n = false; break } } r = false; if (u) { if (f) f.length && c(f.shift());
                        else if (n) u.length = 0;
                        else l.disable() } },
                l = { add: function() { if (u) { var i = u.length,
                                s = function(n) { t.each(n, function(t, n) { if (typeof n === "function") { if (!e.unique || !l.has(n)) u.push(n) } else if (n && n.length && typeof n !== "string") s(n) }) };
                            s(arguments); if (r) a = u.length;
                            else if (n) { o = i;
                                c(n) } } return this }, remove: function() { if (u) { t.each(arguments, function(e, n) { var i; while ((i = t.inArray(n, u, i)) > -1) { u.splice(i, 1); if (r) { if (i <= a) --a; if (i <= s) --s } } }) } return this }, has: function(e) { return !!(u && (e ? t.inArray(e, u) > -1 : u.length)) }, empty: function() { a = u.length = 0; return this }, disable: function() { u = f = n = undefined; return this }, disabled: function() { return !u }, lock: function() { f = undefined; if (!n) l.disable(); return this }, locked: function() { return !f }, fireWith: function(t, e) { if (u && (!i || f)) { e = e || [];
                            e = [t, e.slice ? e.slice() : e]; if (r) f.push(e);
                            else c(e) } return this }, fire: function() { return l.fireWith(this, arguments) }, fired: function() { return !!i } }; return l } })(e);
    (function(t) { var e = t.zepto,
            n = e.qsa,
            i = e.matches;

        function r(e) { e = t(e); return !!(e.width() || e.height()) && e.css("display") !== "none" } var o = t.expr[":"] = { visible: function() { if (r(this)) return this }, hidden: function() { if (!r(this)) return this }, selected: function() { if (this.selected) return this }, checked: function() { if (this.checked) return this }, parent: function() { return this.parentNode }, first: function(t) { if (t === 0) return this }, last: function(t, e) { if (t === e.length - 1) return this }, eq: function(t, e, n) { if (t === n) return this }, contains: function(e, n, i) { if (t(this).text().indexOf(i) > -1) return this }, has: function(t, n, i) { if (e.qsa(this, i).length) return this } }; var a = new RegExp("(.*):(\\w+)(?:\\(([^)]+)\\))?$\\s*"),
            s = /^\s*>/,
            u = "Zepto" + +new Date;

        function f(t, e) { t = t.replace(/=#\]/g, '="#"]'); var n, i, r = a.exec(t); if (r && r[2] in o) { n = o[r[2]], i = r[3];
                t = r[1]; if (i) { var s = Number(i); if (isNaN(s)) i = i.replace(/^["']|["']$/g, "");
                    else i = s } } return e(t, n, i) } e.qsa = function(i, r) { return f(r, function(o, a, f) { try { var c; if (!o && a) o = "*";
                    else if (s.test(o)) c = t(i).addClass(u), o = "." + u + " " + o; var l = n(i, o) } catch (h) { console.error("error performing selector: %o", r); throw h } finally { if (c) c.removeClass(u) } return !a ? l : e.uniq(t.map(l, function(t, e) { return a.call(t, e, l, f) })) }) };
        e.matches = function(t, e) { return f(e, function(e, n, r) { return (!e || i(t, e)) && (!n || n.call(t, null, r) === t) }) } })(e);
    (function(t) { var e = {},
            n, i, r, o, a = 750,
            s;

        function u(t, e, n, i) { return Math.abs(t - e) >= Math.abs(n - i) ? t - e > 0 ? "Left" : "Right" : n - i > 0 ? "Up" : "Down" }

        function f() { o = null; if (e.last) { e.el.trigger("longTap");
                e = {} } }

        function c() { if (o) clearTimeout(o);
            o = null }

        function l() { if (n) clearTimeout(n); if (i) clearTimeout(i); if (r) clearTimeout(r); if (o) clearTimeout(o);
            n = i = r = o = null;
            e = {} }

        function h(t) { return (t.pointerType == "touch" || t.pointerType == t.MSPOINTER_TYPE_TOUCH) && t.isPrimary }

        function p(t, e) { return t.type == "pointer" + e || t.type.toLowerCase() == "mspointer" + e } t(document).ready(function() { var d, m, v = 0,
                g = 0,
                y, b; if ("MSGesture" in window) { s = new MSGesture;
                s.target = document.body } t(document).bind("MSGestureEnd", function(t) { var n = t.velocityX > 1 ? "Right" : t.velocityX < -1 ? "Left" : t.velocityY > 1 ? "Down" : t.velocityY < -1 ? "Up" : null; if (n) { e.el.trigger("swipe");
                    e.el.trigger("swipe" + n) } }).on("touchstart MSPointerDown pointerdown", function(i) { if ((b = p(i, "down")) && !h(i)) return;
                y = b ? i : i.touches[0]; if (i.touches && i.touches.length === 1 && e.x2) { e.x2 = undefined;
                    e.y2 = undefined } d = Date.now();
                m = d - (e.last || d);
                e.el = t("tagName" in y.target ? y.target : y.target.parentNode);
                n && clearTimeout(n);
                e.x1 = y.pageX;
                e.y1 = y.pageY; if (m > 0 && m <= 250) e.isDoubleTap = true;
                e.last = d;
                o = setTimeout(f, a); if (s && b) s.addPointer(i.pointerId) }).on("touchmove MSPointerMove pointermove", function(t) { if ((b = p(t, "move")) && !h(t)) return;
                y = b ? t : t.touches[0];
                c();
                e.x2 = y.pageX;
                e.y2 = y.pageY;
                v += Math.abs(e.x1 - e.x2);
                g += Math.abs(e.y1 - e.y2) }).on("touchend MSPointerUp pointerup", function(o) { if ((b = p(o, "up")) && !h(o)) return;
                c(); if (e.x2 && Math.abs(e.x1 - e.x2) > 30 || e.y2 && Math.abs(e.y1 - e.y2) > 30) r = setTimeout(function() { if (e.el) { e.el.trigger("swipe");
                        e.el.trigger("swipe" + u(e.x1, e.x2, e.y1, e.y2)) } e = {} }, 0);
                else if ("last" in e)
                    if (v < 30 && g < 30) { i = setTimeout(function() { var i = t.Event("tap");
                            i.pageX = e.x2 || e.x1 || 0;
                            i.pageY = e.y2 || e.y1 || 0;
                            i.cancelTouch = l; if (e.el) e.el.trigger(i); if (e.isDoubleTap) { if (e.el) e.el.trigger("doubleTap");
                                e = {} } else { n = setTimeout(function() { n = null; if (e.el) e.el.trigger("singleTap");
                                    e = {} }, 250) } }, 0) } else { e = {} } v = g = 0 }).on("touchcancel MSPointerCancel pointercancel", l);
            t(window).on("scroll", l) });
        ["swipe", "swipeLeft", "swipeRight", "swipeUp", "swipeDown", "doubleTap", "tap", "singleTap", "longTap"].forEach(function(e) { t.fn[e] = function(t) { return this.on(e, t) } }) })(e);
    (function(t) { if (t.os.ios) { var e = {},
                n;

            function i(t) { return "tagName" in t ? t : t.parentNode } t(document).bind("gesturestart", function(t) { var r = Date.now(),
                    o = r - (e.last || r);
                e.target = i(t.target);
                n && clearTimeout(n);
                e.e1 = t.scale;
                e.last = r }).bind("gesturechange", function(t) { e.e2 = t.scale }).bind("gestureend", function(n) { if (e.e2 > 0) { Math.abs(e.e1 - e.e2) != 0 && t(e.target).trigger("pinch") && t(e.target).trigger("pinch" + (e.e1 - e.e2 > 0 ? "In" : "Out"));
                    e.e1 = e.e2 = e.last = 0 } else if ("last" in e) { e = {} } });
            ["pinch", "pinchIn", "pinchOut"].forEach(function(e) { t.fn[e] = function(t) { return this.bind(e, t) } }) } })(e);
    (function(t) { t.fn.end = function() { return this.prevObject || t() };
        t.fn.andSelf = function() { return this.add(this.prevObject || t()) }; "filter,add,not,eq,first,last,find,closest,parents,parent,children,siblings".split(",").forEach(function(e) { var n = t.fn[e];
            t.fn[e] = function() { var t = n.apply(this, arguments);
                t.prevObject = this; return t } }) })(e);
    (function(t) { if (String.prototype.trim === t) String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, "") }; if (Array.prototype.reduce === t) Array.prototype.reduce = function(e) { if (this === void 0 || this === null) throw new TypeError; var n = Object(this),
                i = n.length >>> 0,
                r = 0,
                o; if (typeof e != "function") throw new TypeError; if (i == 0 && arguments.length == 1) throw new TypeError; if (arguments.length >= 2) o = arguments[1];
            else
                do { if (r in n) { o = n[r++]; break } if (++r >= i) throw new TypeError } while (true); while (r < i) { if (r in n) o = e.call(t, o, n[r], r, n);
                r++ } return o } })();
    return t.exports = exports = e
});
define('wq.imk.downloadApp.util', function(require, exports, module) {
    var __cacheThisModule__;
    var Util = {
        "getCookie": function(name) {
            var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
            if (arr = document.cookie.match(reg))
                return decodeURIComponent(arr[2]);
            else
                return null;
        },
        "setCookie": function(name, value, time) { var exp = new Date();
            exp.setTime(exp.getTime() + time);
            document.cookie = name + "=" + encodeURIComponent(value) + ";expires=" + exp.toGMTString(); },
        "urlParse": function(name) {
            var regex = /^((\w+):\/\/)?((\w+):?(\w+)?@)?([^\/\?:]+):?(\d+)?(\/?[^\?#]+)?\??([^#]+)?#?(\w*)/;
            var urlInfo = regex.exec(location.href);
            var param = urlInfo[9];
            if (param && param != "") { var paramInfo = param.split("&"); for (var i = 0; i < paramInfo.length; i++) { var obj = paramInfo[i]; var objInfo = obj.split("="); if (objInfo[0] == name) { return decodeURIComponent(objInfo[1]); } } }
            return '';
        },
        "openJdApp": function(appurl, murl) { var g_sSchema = appurl; var g_sDownload = murl; var g_sUA = navigator.userAgent.toLowerCase(); var jdApp = g_sUA.indexOf('jdapp'); if (jdApp != -1) { location.href = appurl; } else { var div = document.createElement('div');
                div.style.visibility = 'hidden';
                div.innerHTML = "<iframe src=" + g_sSchema + " scrolling=\"no\" width=\"1\" height=\"1\"></iframe>";
                document.body.appendChild(div);
                setTimeout(function() { g_sDownload && (location = g_sDownload); }, 200); } },
        "checkWebp": function() { var imgUrl = "data:image/webp;base64,UklGRjIAAABXRUJQVlA4ICYAAACyAgCdASoCAAEALmk0mk0iIiIiIgBoSygABc6zbAAA/v56QAAAAA=="; var img = new Image();
            img.onload = function() { Util.isWebP = true; };
            img.onerror = function() { Util.isWebP = false; };
            img.src = imgUrl; },
        "isIphone": function() { return navigator.platform === 'iPhone'; },
        "isIpad": function() { return navigator.platform === 'iPad'; },
        "isMac": function() { return navigator.platform === 'MacIntel'; },
        "isAndroid": function() {},
        "isWin32": function() { return navigator.platform === 'Win32'; },
        "ajax": function(prame) {
            var XMLHttpReq;
            try { XMLHttpReq = new ActiveXObject("Msxml2.XMLHTTP"); } catch (e) {
                try { XMLHttpReq = new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) { XMLHttpReq = new XMLHttpRequest(); }
            }
            XMLHttpReq.ajaxRunError = true;
            if (prame.withCredentials) { try { XMLHttpReq.withCredentials = true; } catch (e) {} }
            try { XMLHttpReq.open(prame.method, prame.url, prame.async);
                XMLHttpReq.onreadystatechange = function() { var source = prame.source ? prame.source : null; if (XMLHttpReq.readyState == 4) { if (XMLHttpReq.status == 200) { XMLHttpReq.ajaxRunError = false; var result = XMLHttpReq.responseText;
                            prame.success.call(source, result); } else { prame.error.call(source); } } };
                XMLHttpReq.send('xxxxx=ccccc'); } catch (e) { console.error(e); }
        },
        "autoSetMping": function(eventId, eventParam) {
            if (typeof MPing == 'undefined') { console.error('MPing undefined !!'); return; }
            try {
                var click = new MPing.inputs.Click(eventId);
                if (eventParam) { click.event_param = eventParam; }
                var mping = new MPing();
                mping.send(click);
            } catch (e) { console.error('imk2 send ping error: %o ', e); }
        }
    };
    module.exports = Util;
});
define('wq.imk.downloadApp.logic', function(require, exports, module) {
    var __cacheThisModule__, $ = require('zepto'),
        Util = require('wq.imk.downloadApp.util');
    var Logic = {
        "makeHTML": function(list) {
            var html = '';
            if (list) { var len = list.length; for (var i = 0; i < len; i++) { html += ' <div class="download_btn" data-url="' + list[i].url + '">' + list[i].text + '</div>'; } }
            return html;
        },
        "showDownloadList": function(stateMap) {
            var defaultDownload = [{ url: "//itunes.apple.com/cn/app/id414245413", text: "iPhone版本下载" }, { url: "//apk.360buyimg.com/jdmobile/jd-mxz.apk", text: "Android版本下载" }, { url: "//itunes.apple.com/cn/app/jing-dong-hd/id434374726", text: "iPad版本下载" }, { url: "//m.jd.com", text: "继续浏览京东网页版" }];
            var downloadBtns;
            if (stateMap.ua.iphone) { downloadBtns = [defaultDownload[0]]; } else if (stateMap.ua.android) { downloadBtns = [defaultDownload[1]]; } else if (stateMap.ua.ipad) { downloadBtns = [defaultDownload[2]]; } else { downloadBtns = [defaultDownload[0], defaultDownload[1], defaultDownload[2]]; }
            var html = this.makeHTML(downloadBtns);
            var btns = document.querySelector('#btns');
            if (btns) { btns.innerHTML = html; }
            this.extroDownloadBtns && this.extroDownloadBtns.forEach(function(id) { $('#' + id).attr('data-url', downloadBtns[0].url); });
        },
        "dtetctUA": function() {
            var ua = navigator.userAgent;
            var android = ua.match(/(Android);?[\s\/]+([\d.]+)?/);
            var ipad = ua.match(/(iPad).*OS\s([\d_]+)/);
            var iphone = !ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/);
            if (android) { return { 'android': true }; }
            if (iphone) { return { 'iphone': true }; }
            if (ipad) { return { 'ipad': true }; }
            return {};
        },
        "mPing4Click": function(eventId, eventPara, eventLevel) { if (typeof MPing == 'undefined') {} else { try { var click = new MPing.inputs.Click(eventId);
                    click.event_param = eventPara ? eventPara : "";
                    click.event_level = eventLevel ? parseInt(eventLevel) : ""; var mping = new MPing();
                    mping.send(click); } catch (e) {} } },
        "addDownloadBtn": function(id) { if (!this.extroDownloadBtns) this.extroDownloadBtns = []; if (id) this.extroDownloadBtns.push(id); },
        "showPage": function() {
            var stateMap = { ua: this.dtetctUA(), channel: Util.urlParse('channel'), jdv: (Util.getCookie("__jdv") || "") };
            this.showDownloadList(stateMap);
            var isNewWorld = Util.urlParse("isneworold") || "null";
            var M_sourceFrom = Util.urlParse("M_sourceFrom") || "null";
            var kplsource = Util.urlParse("kplsource") || "null";
            var kplchannel = Util.urlParse("kplchannel") || "null";
            var params = [M_sourceFrom, isNewWorld, kplsource, kplchannel];
            var download_btn = document.querySelectorAll('.download_btn');
            var _this = this;
            for (var i = 0, len = download_btn.length; i < len; i++) {
                download_btn[i].addEventListener('click', function() {
                    var eventId;
                    var url = this.getAttribute("data-url");
                    var text = this.innerText;
                    eventId = 'MDownload_DownloadButton';
                    if (url.indexOf("m.jd.com") != -1) { eventId = 'MGuide_PCWebJD'; if (history.length > 1) { history.back(); } else { setTimeout(function() { location.href = url; }, 200); } } else {
                        var os = Util.urlParse("os") || "";
                        var ua = Util.urlParse("ua") || "";
                        var union = Util.urlParse("union") || "";
                        var channelId = Util.urlParse("channel_id") || "";
                        var appid = Util.urlParse("appid") || "";
                        var ip = Util.urlParse("ip") || "";
                        if (union && channelId && appid) { $.ajax({ 'type': 'GET', 'charset': 'utf8', 'url': '//adcollect.m.jd.com/adcollect.action?os=' + os + '&ua=' + ua + '&unionId=' + union + '&subunionId=' + channelId + '&appid=' + appid + '&ip=' + ip + '&t=' + Math.random() }); }
                        eventId = 'MDownload_DownloadButton';
                        setTimeout(function() { location.href = url; }, 200);
                    }
                    if (!this.getAttribute("report-eventid")) _this.mPing4Click(eventId, text + '_O_button');
                });
            }
            var topImg;
            var uploadImg;
            topImg = document.querySelector('#topImg');
            uploadImg = document.querySelector('#uploadImg');
            topImg.addEventListener('click', function() { _this.mPing4Click('MDownload_Image'); });
            uploadImg.addEventListener('click', function() { _this.mPing4Click('MDownload_Image'); });
            $.ajax({
                'type': 'GET',
                'charset': 'utf8',
                'dataType': 'jsonp',
                'timeout': 1000 * 100,
                'jsonp': 'jsonp',
                'url': '//api.m.jd.com/client.action',
                'data': { functionId: 'clientChannel', body: JSON.stringify({ channelCode: stateMap.channel, jdv: stateMap.jdv, isneworold: Util.urlParse("isneworold") || "", type: params.join("#_#") }), client: "wh5", clientVersion: "1.0.1", uuid: Util.uuid || "" },
                success: function(data) {
                    if (data.code == -1 || data.code == 1) { return; }
                    if (stateMap.ua.android) { var _android = document.querySelector('#btns [data-url$=apk]'); var dUrl = data.downloadUrl.replace(/^https?\:/, '').replace(/\/\/storage\.360buyimg\.com/, '//apk.360buyimg.com');
                        _android.setAttribute('data-url', dUrl); }
                    if (data.imgData) {
                        if (data.imgData.title) { var slogn = document.querySelector('#slogn');
                            slogn.innerText = data.imgData.title; }
                        if (data.imgData.subTitle) { var newUserGift = document.querySelector('#newUserGift');
                            newUserGift.innerText = data.imgData.subTitle; }
                        if (data.imgData.topImg) { var topImg = document.querySelector('#topImg');
                            topImg.src = data.imgData.topImg; }
                        if (data.imgData.uploadImg) { var uploadImg = document.querySelector('#uploadImg');
                            uploadImg.src = data.imgData.uploadImg; }
                        if (data.imgData.bottomImg) { var bottom_baner = document.querySelector('.bottom_baner');
                            bottom_baner.style.background = 'url(' + data.imgData.bottomImg + ') no-repeat;' }
                    }
                },
                error: function(err) { console.error(err); }
            });
        }
    };
    module.exports = Logic;
});
modulejs("wq.imk.downloadApp.logic", function(logic) {
    logic.showPage();
});