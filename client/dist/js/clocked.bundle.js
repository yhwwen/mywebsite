/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(1);

var then = 0;
var delta = 1000;
var fps = 60;
var status = true;
var timeoutId = null;

var log2 = function log2() {
	console.log('foo');
};

var throttleLog = function throttleLog() {
	var now = Date.now();
	if (now - then >= delta) {
		log2();
		then = now;
	}
};

var throttleLogFps = function throttleLogFps() {
	var now = Date.now();
	if (1000 / (now - then) <= fps) {
		log2();
		then = now;
	}
};

var throttleLogStatus = function throttleLogStatus() {
	if (status) {
		log2();
		status = false;

		setTimeout(function () {
			status = true;
		}, delta);
	}
};

var debouncedLog = function debouncedLog() {
	clearTimeout(timeoutId);

	timeoutId = setTimeout(function () {
		log2();
	}, delta);
};

var log = function log() {
	var now = Date.now();
	if (1000 / (now - then) > 60) {
		console.log("It's over 9000!!!");
	}

	then = now;
};

var immediatedLog = function immediatedLog() {
	if (status) {
		log2();
		status = false;
	}

	clearTimeout(timeoutId);
	timeoutId = setTimeout(function () {
		status = true;
	}, delta);
};

var debounce = function debounce(fn, delta, context) {
	var timeoutId = null;
	return function () {
		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		clearTimeout(timeoutId);
		timeoutId = setTimeout(function () {
			fn.apply(context, args);
		}, delta);
	};
};

var immediate = function immediate(fn, delta, context) {
	var timeoutId = null;
	var safe = true;

	return function () {
		for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
			args[_key2] = arguments[_key2];
		}

		if (safe) {
			fn.apply(context, args);
			safe = true;
		}

		clearTimeout(timeoutId);
		timeoutId = setTimeout(function () {
			safe = true;
		}, delta);
	};
};

var throttle = function throttle(fn, delta, context) {
	var safe = true;

	return function () {
		for (var _len3 = arguments.length, args = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
			args[_key3] = arguments[_key3];
		}

		if (safe) {
			fn.apply(context, args);
			safe = false;

			setTimeout(function () {
				safe = true;
			}, delta);
		}
	};
};

document.querySelector('#box01').onmousemove = log;
document.querySelector('#box02').onmousemove = throttleLog;
document.querySelector('#box03').onmousemove = throttleLogFps;
document.querySelector('#box04').onmousemove = throttleLogStatus;
document.querySelector('#text01').onkeydown = debouncedLog;
document.querySelector('#box05').onmousemove = immediatedLog;

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);