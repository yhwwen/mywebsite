const ExtractTextPlugin = require('extract-text-webpack-plugin');
const mobileWidth = 750;
module.exports = [
	{
		test: /\.(js|jsx)$/,
		exclude: /(node_modules|bower_components)/,
		use: {
			loader: 'babel-loader'
		}
	},
	{
		test: /\.css$/,
		use: ExtractTextPlugin.extract({
			fallback: "style-loader",
          	use: [
          		{
          			loader: "css-loader"
          		},
          		{
	            	loader: "postcss-loader"
	            },
          	]
		})
	},
	{
		test: /\.scss$/,
		use: ExtractTextPlugin.extract({
            use: [
	            {
	                loader: "css-loader"
	            }, 
	            {
	            	loader: "postcss-loader"
	            },
	            {
	                loader: "sass-loader"
	            }
            ],
            fallback: "style-loader"
        })
	},
	{
		test: /\.html$/,
		use: {
			loader: 'art-template-loader'
		}
	},
	{
	    test: /\.(woff|woff2|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
	    use: {
	    	loader: 'file-loader?name=fonts/[name].[ext]'
	    }
	},
	{
	    test: /\.(png|jpe?g|gif|cur)$/,
	    use: {
	    	loader: 'url-loader?limit=8192&name=images/[name].[ext]'
	    }
	    
	}
]