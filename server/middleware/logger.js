const log4js = require('log4js');

log4js.configure({
  appenders: {
    out: { type: 'console' },
    app: { 
    	type:"dateFile",
    	filename:"./logs/log",
    	pattern: "_yyyy-MM-dd.log",
    	alwaysIncludePattern: true,
    	absolute: false,
    }
  },
  categories: {
    default: { appenders: [ 'out', 'app' ], level: 'ERROR' }
  }
});


const logger  = log4js.getLogger('app');


module.exports = {
	logger: logger,
	use (app) {
		app.use(log4js.connectLogger(logger, {level:'auto', format:':method :url'}));
	}
};