const fs = require('fs')
const multer = require('multer');

const storage = multer.diskStorage({
	destination (req, file, cb) {
		cb(null, './uploads')
	},

	filename (req, file, cb) {
		const fileFormat = (file.originalname).split(".");
		cb(null, `${file.fieldname }-${Date.now()}.${fileFormat[fileFormat.length - 1]}`)
	}
})

module.exports = multer({
	storage
});