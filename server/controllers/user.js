//const connection  = require('../utils/sql');

const mysql = require('mysql');
const async = require('async');
const pool = mysql.createPool({
	host: 'localhost',
	user: 'root',
	password: '123Qwe',
	database: 'test'
});

// const connection = mysql.createConnection({
// 	host: 'localhost',
// 	user: 'root',
// 	password: '123Qwe',
// 	database: 'test'
// })

pool.on('release', function (connection) {
  console.log('Connection %d released', connection.threadId);
});

let DATA = {
	code: 10000,
	data: {},
	msg: 'success'
};


class User {
	constructor () {

	}

	get (req, res) {
		

		
		let  data  = {
			title: '这是用户页面',
			userList: [{
				id: 1,
				name: "kevin Yang",
				mobile: "13527726592"
			}]
		}
		// connection.connect();
		// connection.query('select * from user', (error, results, fields) => {
		// 	if (error) {
		// 		throw error;
		// 	}

		// 	console.log(results);
		// 	console.log(results[0].name);
		// 	connection.end();
		// 	data.userList = results;
		// 	res.render('user.html', data);
		// })

		// connection.end();

		pool.getConnection((err, connection) => {
			if (err) {
				throw err;
			}
			let execsql = '';

			if (req.params.id) {
				execsql = `SELECT * FROM user WHERE id='${req.params.id}'`;
			} else {
				execsql = `SELECT * FROM user`;
			}

			connection.query(execsql, (error, results, fields) => {
				connection.release();

				console.log(results);
				data.userList = results;
				res.render('user.html', data);
			})
		})
		
	}

	login (req, res) {
		let params = req.body;
		let createConnection = (cb) => {
			pool.getConnection((err, connection) => {
				if (err) {
					throw err;
					cb(err, connection);
				}

				cb(null, connection);
			})
		}

		let isExit = (connection, cb) => {
			let execsql = `SElECT * FROM user WHERE name='${req.body.name}'`
			connection.query(execsql, (error, result, fields) => {
				if (error) {
					throw error;
					cb(err, connection);
				}
				if (result.length) {
					if (result[0].password === params.password) {
						cb(null, connection, result);
					} else {
						cb(new Error('Incorrect name or password'), connection, null);
					}
					
				} else  {
					cb(new Error('Incorrect name or password'), connection, null);
				}	
			})
		}

		let updateItem = (connection, data, cb) => {
			let execsql = `UPDATE user SET lastLogin=NOW() WHERE id=${data[0].id}`;
			connection.query(execsql, (error, result, fields) => {
				if (error) {
					throw error;
					cb(error, connection, null);
				}
				console.log(result);

				cb(null, connection, data);
			});
		};

		let handler = (...args) => {
			let data = {};
			
			args[1].release();
			if (args[0] === null) {
				data = Object.assign({}, DATA, {data: {id: args[2][0].id}});
			} else {
				data = Object.assign({}, DATA, {code: 99999, msg: args[0].toString()})
			}
			res.send(data);
		}

		async.waterfall([
			createConnection,
			isExit,
			updateItem
		], handler);
	}

	register(req, res) {
		
		let params = req.body;
		let createConnection = (cb) => {
			pool.getConnection((err, connection) => {
				if (err) {
					throw err;
					cb(err, connection);
				}

				cb(null, connection);
			})
		}

		let isExit = (connection, cb) => {
			let execsql = `SElECT * FROM user WHERE name='${req.body.name}'`
			connection.query(execsql, (error, results, fields) => {
				if (error) {
					throw error;
					cb(err, connection);
				}
				if (!results.length) {
					cb(null, connection);
				} else  {
					cb(new Error('user is exit'), connection);
				}
				
			})
		}

		let insertItem = (connection, cb) => {
			let execsql = 'INSERT INTO user SET';
			for (let name in params) {
				if (name !== 'id') {
					execsql += ` ${name}='${params[name]}',`
				}
			}

			execsql += ` createTime=NOW()`;
			connection.query(execsql, (error, results, fields) => {
				if (error) {
					throw error;
					cb(error, connection, null);
				}

				cb(null, connection, results);
			})
		}

		let handler = (...args) => {
			let data = {};
			console.log(args[0]);
			args[1].release();
			if (args[0] === null) {
				data = Object.assign({}, DATA, {data: args[2][0]});
			} else {
				data = Object.assign({}, DATA, {code: 99999, msg: args[0].toString()})
			}
			res.send(data);
		}

		async.waterfall([
			createConnection,
			isExit,
			insertItem
		], handler);
	}

	delete (req, res) {
		console.log(req.body);

		let createConnection = (cb) => {
			pool.getConnection((err, connection) => {
				if (err) {
					throw err;
					cb(err, connection);
				}

				cb(null, connection);
			})
		}

		let isExit = (connection, cb) => {
			let execsql = `SElECT * FROM user WHERE id='${req.params.id}'`
			connection.query(execsql, (error, results, fields) => {
				if (error) {
					throw error;
					cb(err, connection);
				}
				if (results.length) {
					cb(null, connection);
				} else  {
					cb(new Error('user is not exit'), connection);
				}
				
			})
		}

		let deleteItem = (connection, cb) => {
			let execsql = `DELETE FROM user WHERE id='${req.params.id}'`;

			connection.query(execsql, (error, results, fields) => {
				if (error) {
					throw error;
					cb(error, connection);
				}
				cb(null, connection);
			})
		}

		let queryResut = (connection, cb) => {
			let execsql = `SElECT * FROM user`;
			connection.query(execsql, (error, results, fields) => {
				if (error) {
					throw error;
					cb(err, connection, null);
				}

				cb(null, connection, results);
			})
		}


		let handler = (...args) => {
			let data = {};
			console.log(args[0]);
			args[1].release();
			if (args[0] === null) {
				data = Object.assign({}, DATA, {data: args[2]});
			} else {
				data = Object.assign({}, DATA, {code: 99999, msg: args[0].toString()})
			}
			res.send(data);

		}

		async.waterfall([
			createConnection,
			isExit,
			deleteItem,
			queryResut
		], handler);
	}

	post (req, res) {
		console.log(req.body);

		let params = req.body;

		for(let name in params) {
			if (!params[name]) {
				delete params[name]
			}
		}

		let createConnection = (cb) => {
			pool.getConnection((err, connection) => {
				if (err) {
					throw err;
					cb(err, connection);
				}

				cb(null, connection);
			})
		}

		let isExit = (connection, cb) => {
			let execsql = `SElECT * FROM user WHERE mobile='${params.mobile}'`
			connection.query(execsql, (error, results, fields) => {
				if (error) {
					throw error;
					cb(err, connection);
				}
				if (!results.length) {
					cb(null, connection);
				} else  {
					cb(new Error('Mobile isExit'), connection);
				}
				
			})
		}

		let insertItem = (connection, cb) => {
			let execsql = 'INSERT INTO user SET ?';
			connection.query(execsql, params, (error, results, fields) => {
				if (error) {
					throw error;
					cb(error, connection, null);
				}
				cb(null, connection, results);
			})
		}

		let updateItem = (connection, cb) => {
			let execsql = 'UPDATE user SET';
			for (let name in params) {
				if (name !== 'id') {
					execsql += ` ${name}='${params[name]}',`
				}
			}

			execsql = execsql.substring(0, execsql.length - 1);
			execsql += ` WHERE id=${params.id}`;
			connection.query(execsql, (error, results, fields) => {
				if (error) {
					throw error;
					cb(error, connection, null);
				}

				cb(null, connection, {insertId: params.id});
			})
		};

		let queryResut = (connection, results, cb) => {
			console.log(results);
			let execsql = `SElECT * FROM user WHERE id='${results.insertId}'`;
			connection.query(execsql, (error, results, fields) => {
				if (error) {
					throw error;
					cb(err, connection, null);
				}

				cb(null, connection, results);
			})
		}

		let handler = (...args) => {
			let data = {};
			console.log(args[0]);
			args[1].release();
			if (args[0] === null) {
				data = Object.assign({}, DATA, {data: args[2][0]});
			} else {
				data = Object.assign({}, DATA, {code: 99999, msg: args[0].toString()})
			}
			res.send(data);
		}

		if (!params.id) {
			async.waterfall([
				createConnection,
				isExit,
				insertItem,
				queryResut
			], handler);
		} else {
			async.waterfall([
				createConnection,
				isExit,
				updateItem,
				queryResut
			], handler);
		}
	}
}


module.exports = new User();