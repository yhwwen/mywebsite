const express = require('express');
const router = express.Router();
const ctrl = require('../controllers');   


router
    .get('/', ctrl.react.get)
    .get('/:page', ctrl.react.get)

module.exports = router;