const express = require('express');
const router = express.Router();
const ctrl = require('../controllers');


router.get('/', ctrl.clocked.get)

module.exports = router;