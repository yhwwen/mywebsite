const express = require('express');
const router = express.Router();
const ctrl = require('../controllers');


const upload = require('../middleware/uploader');


router
    .post('/photo', upload.single('file'), ctrl.upload.photo)
    .get('/', ctrl.upload.get)
    .post('/', upload.single('file'), ctrl.upload.post)


module.exports = router;