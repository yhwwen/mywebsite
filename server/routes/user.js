const express = require('express');
const router = express.Router();
const ctrl = require('../controllers');   


router
    .get('/', ctrl.user.get)
	.get('/:id', ctrl.user.get)
	.post('/', ctrl.user.post)
    .delete('/:id', ctrl.user.delete)
    .post('/login', ctrl.user.login)
    .post('/register', ctrl.user.register)

module.exports = router;