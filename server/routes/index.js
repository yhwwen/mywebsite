const express = require('express')
const router = express.Router()
const _ = require('lodash')
const fs = require('fs')
const path = require('path')

router.use((req, res, next) => {
	console.log(`Time:${Date.now()}`);
	next();
})

const mapDir = d => {
	const tree = {};

	// 获得当前文件夹下的所有的文件夹和文件
	const [dirs, files] = _(fs.readdirSync(d))
							.partition(p => fs.statSync(path.join(d, p)).isDirectory())

	// 映射文件夹
	dirs.forEach(dir => {
	    tree[dir] = mapDir(path.join(d, dir))
	})

	// 映射文件
	files.forEach(file => {
		if (path.extname(file) === '.js') {
			const key = path.basename(file, '.js');
			tree[key] = require(path.join(d, file));
			if (typeof tree[key] === 'function') {
				const url = key === "home" ? '/' : ('/' + key)
				router.use(url, tree[key]);
			}
		}
	})
}


mapDir(path.join(__dirname));

module.exports = router;