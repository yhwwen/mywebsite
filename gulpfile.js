const gulp = require('gulp')
const nodemon = require('gulp-nodemon')
const browserSync = require('browser-sync').create()
const reload = browserSync.reload;
const babel = require('gulp-babel')
const replace = require('gulp-replace')
const sass = require('gulp-sass')
const minifyCss = require('gulp-clean-css')
const source = require('vinyl-source-stream')
const rename = require('gulp-rename')
const browserify = require('browserify')
const glob = require('glob')
const es = require('event-stream')
const plumber = require('gulp-plumber')
const notify = require("gulp-notify")
const jshint = require('gulp-jshint')
const stylish = require('jshint-stylish')
const postcss = require('gulp-postcss');
const clean = require('gulp-clean');
const rev = require('gulp-rev');
const revCollector = require('gulp-rev-collector');
const buffer = require('gulp-buffer');
const pxtorem = require('postcss-pxtorem');
const autoprefixer = require('autoprefixer');
const mobileWidth = 750;
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');
const webpack = require('webpack');
//const webpackConfig = require('./webpack.config.js');
const path = require('path');
const _root = path.resolve(__dirname);
const outputdir = path.join(_root, './client/dist');
const outputPublicPath = '/';
const loaders = require('./config/loader');


gulp.task('default', ['nodemon', 'serve', 'sass:server:all', 'sass:server:H5:all', 'javascript:server:all', 'watch:server']);
gulp.task('server', ['nodemon', 'serve', 'sass:server:all', 'javascript:server:all', 'watch:server:web'])
gulp.task('server_h5', ['nodemon', 'serve', 'sass:server:H5:all', 'javascript:server:all', 'watch:server:H5'])
gulp.task('dev', ['nodemon', 'serve', 'static', 'webpack'])
gulp.task('dev:webpack', ['nodemon', 'serve', 'webpack'])
gulp.task('publish', ['clean', 'build', 'build:html'])



/*gulp.task('webpack', function (callback) {
    webpack({
        entry: {
            index: './client/src/javascript/pages/index.js'
        },
        output: {
            path: outputdir,
            filename: 'js/[name].web.js?v=[hash]',
            publicPath: outputPublicPath
        },
        module: {
            rules: loaders
        },
        plugins: []
    }, function (err, stats) {
        
    })
})*/

gulp.task('webpack', function (done) {
    glob('./client/src/pages/**/*.js', function (err, files) {
        if (err) {
            done(err);
        }

        //console.log(files);
        files.forEach(entry => {
            packJs(entry);
        })
        /*var tasks = files.map(function (entry) {
            return handlerJS(entry);
            });
        es.merge(tasks).on('end', done);*/
    })
})


const packJs = (entry) => {
    const reg = /\.\/client\/src\/pages\/(.+)\.js$/;
    const matcher = entry.match(reg);
    if (matcher) {
        const jsPathName = matcher[1];
        const htmlTemplate = entry.replace(/\.js$/, '.html');
        const entries = {};
        entries[jsPathName] = entry;
        webpack({
            watch: true,
            cache: true,
            entry: entries,
            output: {
                path: outputdir,
                filename: 'js/[name].bundle.js?v=[hash]',
                publicPath: outputPublicPath 
            },
            module: {
                rules: loaders
            },
            plugins: [
                new WebpackNotifierPlugin({
                    title: jsPathName,
                    excludeWarnings: true,
                    skipFirstNotification: true
                }),
                new webpack.NoErrorsPlugin(),
                new HtmlWebpackPlugin({
                    template: htmlTemplate,
                    filename: path.resolve(outputdir, 'views/' + jsPathName + '.html'),
                    // filename: htmlPathName + '.html',
                    inject: 'body',
                    chunks: [jsPathName],
                    hash: false
                }),
                new ExtractTextPlugin("css/[name].css?v=[hash]"),
            ]

        }, function(err, stats) {
            if (err) {
                throw err;
            }

            console.log('done')
            //webpack 处理完成这里执行回调
            reload({stream: true});
        })
    }
    /*webpack({
        entry: {
            index: './client/src/javascript/pages/index.js'
        },
        output: {
            path: outputdir,
            filename: 'js/[name].web.js?v=[hash]',
            publicPath: outputPublicPath
        },
        module: {
            rules: loaders
        },
        plugins: []
    }, function (err, stats) {
        
    })*/
};

gulp.task('nodemon', function () {
	return nodemon({
		ext: 'js json',
		env: {
			'NODE_ENV': 'development'
		},
		ignore: [
			'.git',
			'client/*.*',
			'node_modules/**/node_modules',
			'nodemon.json',
			'gulpfile.js'
		],
		restartable: "rs",
		script: 'app.js',
		watch: [
	    	'server/',
            'app.js'
	    ]
	})
})

const handlerH5Sass = (paths) => {
	const processors = [
            autoprefixer({
                browsers: ['last 4 version','Android >= 4.0', 'Chrome > 44', 'Firefox > 20'],//添加浏览器最近的四个版本需要的前缀，兼容安卓4.0以上版本
                cascade: false,//是否美化属性,默认true
                remove: true//移除不必要的前缀
            }),
            pxtorem({
        		rootValue: mobileWidth/10,
        		unitPrecision: 3,
                propList: ['*', '!border', '!border-radius'],
                minPixelValue: 3,
                selectorBlackList: ['html']
        	})];
	gulp.src([paths])
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(plumber.stop())  
        .pipe(minifyCss())
        .pipe(rename(function (path) {
            path.basename += '.min';
            path.extname = '.css';
        }))
        .pipe(gulp.dest('./client/dist/style'))
        .pipe(notify('Done!'))
        .pipe(reload({stream: true}))
}

const handlerSass  = (paths) => {
    const processors = [
            autoprefixer({
                browsers: ['last 4 version','Android >= 4.0', 'Chrome > 44', 'Firefox > 20'],//添加浏览器最近的四个版本需要的前缀，兼容安卓4.0以上版本
                cascade: false,//是否美化属性,默认true
                remove: true//移除不必要的前缀
            })];
	gulp.src([paths])
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(processors))    
        .pipe(minifyCss())
        .pipe(plumber.stop())
        .pipe(gulp.dest('./client/dist/css'))
        .pipe(notify('Done!'))
        .pipe(reload({stream: true}))
}

const handlerJS = (entry) => {
	let modifyDirname = '';
	if (entry.indexOf(__dirname) === -1) {
		modifyDirname = entry
						.replace(`./client/src/js`, '')
                        .replace(/\/[A-Za-z0-9\-\_\.]+\.js$/, '');
	} else {
		modifyDirname = entry
						.replace(`${__dirname}\\client\\src\\js`, '')
                        .replace(/\\[A-Za-z0-9\-\_\.]+\.js$/, '');
	}
	return browserify({ entries: [entry]})
    .transform('babelify', {
        presets: [
            'es2015',
            'stage-0',
        ],
        plugins: [
            'transform-object-assign',
            ['transform-es2015-classes', {"loose": false}],
            ['transform-es2015-modules-commonjs', {"loose": false}]
        ]
    })
    .bundle()
    .on('error', function (error) {
        console.log(error.message);
        gulp.src(entry)
            .pipe(notify({
                title: "Javascript Error",
                message: "File is, " + entry,
                emitError: true
            }))
            .pipe(jshint())
            .pipe(jshint.reporter(stylish))    
        this.emit('end');
    })
    .pipe(source(entry))
    .pipe(rename(function (path) {
        path.dirname = "";
        path.basename += '.bundle';
        path.extname = '.js';
    }))
    .pipe(gulp.dest('./client/dist/javascript'))
}


gulp.task('sass:server:all', function () {
    return handlerSass('./client/src/sass/**/*.scss')
})

gulp.task('sass', function () {
    return handlerH5Sass('./client/src/sass_h5/**/*.scss')
})

gulp.task('html', function () {
    return gulp.src('./client/src/views/**/*.html')
            .pipe(gulp.dest('./client/dist/views'))
            .pipe(reload({stream: true}));
})

gulp.task('sass:server:H5:all', function () {
    return handlerH5Sass('./client/src/sass_h5/**/*.scss')
})

gulp.task('javascript:server:all', function (done) {
    glob('./client/src/javascript/pages/**/*.js', function (err, files) {
        if (err) {
            done(err);
        }
        var tasks = files.map(function (entry) {
            return handlerJS(entry);
            });
        es.merge(tasks).on('end', function () {
            notify('Done!');
            reload({stream: true})
        });
    })
});

gulp.task('js', function (done) {
    glob('./client/src/javascript/pages/**/*.js', function (err, files) {
        if (err) {
            done(err);
        }
        var tasks = files.map(function (entry) {
            return handlerJS(entry);
            });
        es.merge(tasks).on('end', function () {
            notify('Done!');
            reload({stream: true})
        });
    })
})

//gulp.task('js', ['js:hash', 'js:es5'])

gulp.task('clean', ['clean:rev', 'clean:js', 'clean:css']);

gulp.task('clean:rev', function () {
    return gulp.src('./client/src/rev/**/*.json', {read: false})
            .pipe(clean())
})

gulp.task('clean:js', function () {
    return gulp.src(['./client/src/js/**/*.js', './client/dist/javascript/**/*.js'], {read: false})
            .pipe(clean())
})

gulp.task('clean:css', function () {
    return gulp.src('./client/dist/style/**/*.css', {read: false})
            .pipe(clean())
})

gulp.task('build', ['build:css', 'build:js'])

gulp.task('build:js', ['js:hash', 'js:es5']);

gulp.task('build:css', function () {
    const processors = [
            autoprefixer({
                browsers: ['last 4 version','Android >= 4.0', 'Chrome > 44', 'Firefox > 20'],//添加浏览器最近的四个版本需要的前缀，兼容安卓4.0以上版本
                cascade: false,//是否美化属性,默认true
                remove: true//移除不必要的前缀
            }),
            pxtorem({
                rootValue: mobileWidth/10,
                unitPrecision: 3,
                propList: ['*', '!border', '!border-radius'],
                minPixelValue: 3,
                selectorBlackList: ['html']
            })];
    return  gulp.src('./client/src/sass_h5/**/*.scss')
                .pipe(plumber({
                    errorHandler: notify.onError("Error: <%= error.message %>")
                }))
                .pipe(sass().on('error', sass.logError))
                .pipe(postcss(processors))
                .pipe(plumber.stop())  
                .pipe(minifyCss())
                .pipe(rename(function (path) {
                    path.basename += '.min';
                    path.extname = '.css';
                }))
                .pipe(rev())
                .pipe(gulp.dest('./client/dist/style'))
                .pipe(rev.manifest())
                .pipe(gulp.dest('./client/src/rev/css'))
})

gulp.task('build:html', function () {
    return gulp.src(['./client/src/rev/**/*.json', './client/src/views/**/*.html'])
        .pipe(revCollector({
            replaceReved: true
        }))
        .pipe(gulp.dest('./client/dist/views'))
})


gulp.task('js:hash', function () {
    return gulp.src('./client/src/javascript/pages/**/*.js')
        .pipe(rename(function (path) {
            path.basename += '.bundle';
            path.extname = '.js';
        }))
        .pipe(rev())
        .pipe(gulp.dest('./client/src/js'))
        .pipe(rev.manifest())
        .pipe(gulp.dest('./client/src/rev/js'))
})

gulp.task('js:es5', function (callback) {
    glob('./client/src/js/**/*.js', function (err, files) {
        if (err) {
            done(err);
        }
        var tasks = files.map(function (entry) {
            return handlerJS(entry);
            });
        es.merge(tasks).on('end', function () {
            callback()
        });
    })
})

gulp.task('watch:server:javascripts', function () {
	return gulp.watch('./client/src/javascript/**/*.js', function (event) {
		handlerJS(event.path)
	})
})

gulp.task('watch:js', function () {
    return gulp.watch('./client/src/javascript/**/*.js', function (event) {
        handlerJS(event.path)
    })
})


gulp.task('watch:html', function () {
    return gulp.watch('./client/src/views/**/*.html', function (event) {
        gulp.src([event.path])
            .pipe(gulp.dest('./client/dist/views'))
            .pipe(reload({stream: true}));
    })
})

gulp.task('static', ['clean', 'start', 'watch'])

gulp.task('start', ['html', 'sass', 'js'])

gulp.task('watch', ['watch:html', 'watch:sass', 'watch:js']);

gulp.task('watch:server:sass', function () {
	return gulp.watch('./client/src/**/*.scss', function (event) {
	    handlerSass(event.path);
	})
})

gulp.task('watch:sass', function () {
    return gulp.watch('./client/src/**/*.scss', function (event) {
        handlerSass(event.path);
    })
})

gulp.task('watch:server:H5:sass', function () {
	return gulp.watch('./client/src/sass_h5/**/*.scss', function (event) {
	    handlerH5Sass(event.path);
	})
})
gulp.task('watch:server', ['watch:server:javascripts', 'watch:server:sass', 'watch:server:H5:sass'])
gulp.task('watch:server:web', ['watch:server:javascripts', 'watch:server:sass'])
gulp.task('watch:server:H5', ['watch:server:javascripts', 'watch:server:H5:sass'])

gulp.task('serve', function () {
	browserSync.init({
		proxy: 'http://localhost:8001'
	})
	//gulp.watch('client/src/views/**/*.html').on('change', reload)
	gulp.watch('server/controllers/**/*.*').on('change', reload)
	gulp.watch('server/routes/**/*.*').on('change', reload)
})