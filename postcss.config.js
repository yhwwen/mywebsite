const mobileWidth = 750;

module.exports = {
	plugins: {
		"autoprefixer": {
			browsers: ["last 4 version", "Android >= 4.0", "Chrome > 44", "Firefox > 20"], //添加浏览器最近的四个版本需要的前缀，兼容安卓4.0以上版本
            cascade: false,		//是否美化属性,默认true
            remove: true		//移除不必要的前缀
		},
		/*"postcss-pxtorem": {
			rootValue: mobileWidth/10,
            unitPrecision: 3,
            propList: ['*', '!border', '!border-radius'],
            minPixelValue: 3,
            selectorBlackList: ['html']
		}*/
	}
}