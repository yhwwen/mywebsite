const path = require('path');
const _root = path.resolve(__dirname);
const outputdir = path.join(_root, './client/dist');
const outputPublicPath = '/';
const loaders = require('./config/loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');

module.exports = {
	watch: true,
    cache: true,
	entry: {
		react: './client/src/react/index.js'
	},
	output: {
		path: outputdir,
		filename: 'js/[name].bundle.js?v=[hash:8]',
		publicPath: outputPublicPath
	},
	module: {
		rules: loaders
	},
	plugins: [
		new WebpackNotifierPlugin({
			title: "Error",
			excludeWarnings: true,
            skipFirstNotification: true
		}),
		new HtmlWebpackPlugin({
			template: path.resolve(_root, './client/src/react/index.html'),
			filename: path.resolve(_root, './client/dist/views/react.html'),
			inject: true
		}),
		new ExtractTextPlugin("css/[name].css?v=[hash]"),
	]
}